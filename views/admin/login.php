<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">

	<link rel="shortcut icon" href="<?php echo base_url('assets/'); ?>images/icons/favicon.ico">
	<link rel="apple-touch-icon" href="<?php echo base_url('assets/'); ?>images/icons/favicon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url('assets/'); ?>images/icons/favicon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url('assets/'); ?>images/icons/favicon-114x114.png">
	<!--Loading bootstrap css-->
	<link type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,800italic,400,700,800">
	<link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Oswald:400,700,300">
	<link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/'); ?>vendors/jquery-ui-1.10.3.custom/css/ui-lightness/jquery-ui-1.10.3.custom.css">
	<link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/'); ?>vendors/font-awesome/css/font-awesome.min.css">
	<link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/'); ?>vendors/bootstrap/css/bootstrap.min.css">
	<!--Loading style vendors-->
	<link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/'); ?>vendors/animate.css/animate.css">
	<link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/'); ?>vendors/iCheck/skins/all.css">
	<!--Loading style-->
	<link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/'); ?>css/themes/style1/pink-violet.css" id="theme-change" class="style-change color-change">
	<link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/'); ?>css/style-responsive.css">
</head>
<body id="signin-page" style="height:auto">
<div class="page-form">
	<form class="form" id="frmlogin">
		<div class="header-content"><h1>Log In</h1></div>

		<div class="body-content text-center">
			<div id="msginfo" class="text-danger"></div>
			<div id="msgwarn" class="text-warning"></div>
			<div id="msgok" class="text-success"></div>
		</div>
		<div class="clearfix"></div>
		<div class="body-content">
			<?php 
			$csrf = array(
				'name' => $this->security->get_csrf_token_name(),
				'hash' => $this->security->get_csrf_hash()
			);
			?>
			<input type="hidden" id="csrf" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
			<div class="form-group">
				<div class="input-icon right"><i class="fa fa-user"></i><input type="text" placeholder="Username" id="username" name="username" class="form-control" value="<?php echo $unamex;?>"></div>
			</div>
			<div class="form-group">
				<div class="input-icon right"><i class="fa fa-key"></i><input type="password" placeholder="Password"  id="password" name="password" class="form-control"></div>
			</div>
			<div class="form-group pull-left">
				<div class="checkbox-list"><label><input type="checkbox"  id="rme" name="rme" <?php echo ($unamex != '')?'checked':''; ?>>&nbsp;Keep me signed in</label></div>
			</div>
			<div class="form-group pull-right">
				<button type="button" class="btn btn-success" id="btnlogin">Log In &nbsp;<i class="fa fa-chevron-circle-right"></i></button>
			</div>
			<div class="clearfix"></div>
			<!--<div class="forget-password"><h4>Forgotten your Password?</h4>
				<p>no worries, click <a href='#' class='btn-forgot-pwd'>here</a> to reset your password.</p>
			</div>-->
		</div>
	</form>
</div>

<script src="<?php echo base_url('assets/'); ?>js/jquery-1.10.2.min.js"></script>
<script src="<?php echo base_url('assets/'); ?>js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?php echo base_url('assets/'); ?>js/jquery-ui.js"></script>
<!--loading bootstrap js-->
<script src="<?php echo base_url('assets/'); ?>vendors/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url('assets/'); ?>vendors/bootstrap-hover-dropdown/bootstrap-hover-dropdown.js"></script>
<script src="<?php echo base_url('assets/'); ?>js/html5shiv.js"></script>
<script src="<?php echo base_url('assets/'); ?>js/respond.min.js"></script>
<script src="<?php echo base_url('assets/'); ?>vendors/iCheck/icheck.min.js"></script>
<script src="<?php echo base_url('assets/'); ?>vendors/iCheck/custom.min.js"></script>
<script src="<?php echo base_url('assets/'); ?>js/app.js"></script>
<script>
var BaseURL = '<?php echo BASE; ?>';
$('input[type="checkbox"]').iCheck({
	checkboxClass: 'icheckbox_minimal-grey',
	increaseArea: '20%' // optional
});
$('input[type="radio"]').iCheck({
	radioClass: 'iradio_minimal-grey',
	increaseArea: '20%' // optional
});

$(document).ready(function(){
	$('#btnlogin').click(function(e){
		e.preventDefault();
		UrlPost = BaseURL+'api/admin/login';
		isiPost = $('#frmlogin').serialize();
		aksiFormAJAXjson(UrlPost, isiPost, function(data){
			console.log(data);
			
			if(data.status === true) {
				window.location.replace(BaseURL+'admin/dashboard');
			}
			else {
				if(data.status == 'banned')
					setTimeout("location.reload(true);", 5000);
				
				$('#msginfo').html(data.message);
				$('#csrf').attr('name', data.csrf.name);
				$('#csrf').val(data.csrf.hash);
			}
		})
	})
})
</script>
</body>
</html>