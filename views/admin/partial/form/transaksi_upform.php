<div class="page-content">
	<div class="row">
		<div class="col-lg-12">
			<div class="portlet box portlet-green">
				<div class="portlet-header">
						<div class="caption">Input Transaksi</div>
				</div>
				<div class="portlet-body">
						<?php echo (validation_errors())?'<div class="alert alert-danger">'.validation_errors().'</div>':''; ?>
						<?php echo form_open('admin/form/add_transaksi'); ?>
								<div class="row">
									<div class="col-md-4">

										<div class="form-group"><label for="NoResi">No Resi</label><input type="text" id="NoResi" name="no_resi" placeholder="No Resi" class="form-control" value="<?php echo set_value('no_resi'); ?>" required/></div>
										<div class="form-group"><label for="IsiBrg">Isi Barang</label><input type="text" id="IsiBrg" name="isi_barang" placeholder="Isi Barang" class="form-control" value="<?php echo set_value('isi_barang'); ?>" required/></div>
										<div class="form-group"><label for="Koli">Koli</label><input type="text" id="Koli" name="koli" placeholder="Koli" class="form-control" value="<?php echo set_value('koli'); ?>"/></div>
										<div class="form-group"><label for="Berat">Berat</label>
											<div class="input-group"><span class="input-group-addon"><i class="fa fa-gift"></i></span><input type="text" placeholder="Berat" id="Berat" name="berat" class="form-control" value="<?php echo set_value('berat'); ?>"/></div>
										</div>	

										<div class="form-group"><label for="CaraByr">Cara Bayar</label>
											<select id="CaraByr" name="cara_bayar" class="form-control">
												<option value="CASH">CASH</option>
												<option value="CREDIT">CREDIT</option>
												<option value="TEMPORER">TEMPORER</option>
											</select>
										</div>
										<div class="form-group"><label for="KdTarif">Kode Tarif</label>
											<select id="KdTarif" name="kode_tarif" class="form-control">
												<option value="">--- Pilih Kode Tarif ---</option>
												<?php 
												if(!empty($list_tarif)):
													foreach($list_tarif as $item): ?>	
												<option value="<?php echo $item->id_tarif; ?>" <?php echo  set_select('customer', $item->id_tarif); ?>><?php echo $item->kode_tarif.' - '.$item->kode_pos; ?></option>
												<?php 
													endforeach;
												endif; ?>
											</select>
											<br>
											<button type="button" id="AddTarif" class="btn btn-sm btn-green btnadd" data-tipe="tarif"> Add </button>
											<hr>
										</div>

										<div class="form-group"><label for="TtlByr">Total Bayar</label>
											<input type="text" id="TtlByr" name="total_bayar" placeholder="Total Bayar" class="form-control" value="<?php echo set_value('total_bayar'); ?>"/>
										</div>
										<div class="form-group"><label for="SisaByr">Sisa Bayar</label>
											<input type="text" id="SisaByr" name="sisa_bayar" placeholder="Sisa Bayar" class="form-control" value="<?php echo set_value('sisa_bayar'); ?>"/>
										</div>
									</div>

									<div class="col-md-4">
										<div class="form-group"><label>Pegirim (Customer)</label>
											<select id="IdPengirim" name="id_pengirim" class="form-control">
												<option value="">--- Pilih Customer ---</option>
												<?php 
												if(!empty($list_customer)):
													foreach($list_customer as $item): ?>	
												<option value="<?php echo $item->id_customer; ?>" <?php echo  set_select('customer', $item->id_customer); ?>><?php echo $item->kode_pelanggan.' - '.$item->nama_pelanggan; ?></option>
												<?php 
													endforeach;
												endif; ?>
											</select>
											<br>
											<button type="button" id="AddPegirim" class="btn btn-sm btn-green btnadd" data-tipe="pengirim"> Add </button>
											<hr>
										</div>

										<div class="form-group"><label for="PgEmail">Detail</label>
											<div class="input-group"><span class="input-group-addon"><i class="fa fa-envelope"></i></span><input type="text" placeholder="Email" id="PgEmail" name="pg_email" class="form-control" readonly="readonly" value="<?php echo set_value('pg_email'); ?>"/></div>
											<div class="input-group"><span class="input-group-addon"><i class="fa fa-phone"></i></span><input type="text" placeholder="Telepon" id="PgTelepon" name="pg_telepon" class="form-control" readonly="readonly" value="<?php echo set_value('pg_telepon'); ?>"/></div>
											<div class="input-group"><span class="input-group-addon"><i class="fa fa-fax"></i></span><input type="text" placeholder="Fax" id="PgFax" name="pg_fax" class="form-control" readonly="readonly" value="<?php echo set_value('pg_fax'); ?>"/></div>
											<div class="form-input"><label>Alamat</label><textarea rows="5" id="PgAlamat" name="pg_alamat" class="form-control" readonly="readonly"><?php echo set_value('pg_alamat'); ?></textarea></div>
										</div>
										<div class="form-group"><label>Service</label>
											<input type="text" id="Service" name="service" placeholder="Readonly" readonly="readonly" class="form-control" value="<?php echo set_value('service'); ?>"/>
										</div>
										<div class="form-group"><label for="Tarif">Tarif Layanan</label>
											<select id="Tarif" name="tarif" class="form-control">
												<option value="">--- Pilih Tarif Layanan ---</option>
											</select>
										</div>
										<div class="form-group"><label for="Diskon">Diskon Layanan</label>
											<input type="text" id="Diskon" name="diskon" placeholder="Readonly" readonly="readonly" class="form-control" value="<?php echo set_value('diskon'); ?>"/>
										</div>
									</div>

									<div class="col-md-4">
										<div class="form-group"><label for="Penerima">Penerima</label>
											<select id="IdPenerima" name="id_penerima" class="form-control">
												<option value="">--- Pilih Penerima ---</option>
											<?php 
											if(!empty($list_penerima)):
												foreach($list_penerima as $item): ?>	
											<option value="<?php echo $item->id_penerima; ?>" <?php echo  set_select('customer', $item->id_penerima); ?>><?php echo $item->kode_pos.' - '.$item->nama_penerima; ?></option>
											<?php 
												endforeach;
											endif; ?>
											</select>
											<br>
											<button type="button" id="AddPenerima" class="btn btn-sm btn-green btnadd" data-tipe="penerima"> Add </button>
											<hr>
										</div>

										<div class="form-group"><label for="PnKodepos">Detail</label>
											<div class="input-group"><span class="input-group-addon"><i class="fa fa-globe"></i></span><input type="text" placeholder="Kodepos" id="PnKodepos" name="pn_kodepos" class="form-control" readonly="readonly" value="<?php echo set_value('pn_kodepos'); ?>"/></div>
											<div class="input-group"><span class="input-group-addon"><i class="fa fa-globe"></i></span><input type="text" placeholder="Kota Tujuan" id="PnKota" name="pn_kota" class="form-control" readonly="readonly" value="<?php echo set_value('pn_kota'); ?>"/></div>
											<div class="input-group"><span class="input-group-addon"><i class="fa fa-envelope"></i></span><input type="text" placeholder="Email" id="PnEmail" name="pn_email" class="form-control" readonly="readonly" value="<?php echo set_value('pn_email'); ?>"/></div>
											<div class="input-group"><span class="input-group-addon"><i class="fa fa-phone"></i></span><input type="text" placeholder="Telepon" id="PnTelepon" name="pn_telepon" class="form-control" readonly="readonly" value="<?php echo set_value('pn_telepon'); ?>"/></div>
											<div class="input-group"><span class="input-group-addon"><i class="fa fa-fax"></i></span><input type="text" placeholder="Fax" id="PnFax" name="pn_fax" class="form-control" readonly="readonly" value="<?php echo set_value('pn_fax'); ?>"/></div>
											<div class="form-input"><label for="Alamat">Alamat</label><textarea id="PnAlamat" name="pn_alamat" rows="5" class="form-control" readonly="readonly"><?php echo set_value('pn_alamat'); ?></textarea></div>
										</div>

										<div class="form-group"><label>Tanggal pengiriman</label>
											<input type="text" placeholder="Tanggal Pengiriman" id="TglKirim" name="tgl_kirim" class="form-control" value="<?php echo set_value('tgl_kirim'); ?>"/>
										</div>

										<div class="form-group"><label>Estimasi sampai</label>
											<input type="text" placeholder="Etimasi Barang Sampai" id="EstSampai" name="est_sampai" class="form-control" value="<?php echo set_value('est_sampai'); ?>"/>
										</div>

									</div>
								</div>
		
								<div class="row">
									<div class="col-md-12">
											<div class="form-actions">
												<div class="text-center">
													<button type="submit" class="btn btn-green">Submit</button>
													&nbsp;
													<button type="reset" class="btn btn-default">Cancel</button>
												</div>
											</div>
										</div>
								</div>

						</form>
				</div>
			</div>
		</div>
	</div>
</div>