<div class="page-content">
	<div class="row">
		<div class="col-lg-12">
			<div class="portlet box portlet-green">
				<div class="portlet-header">
						<div class="caption">Tambah Customer</div>
				</div>
				<div class="portlet-body">
						<?php echo (validation_errors())?'<div class="alert alert-danger">'.validation_errors().'</div>':''; ?>
						<?php echo form_open('admin/form/add_customer'); ?>
								<div class="row">
										<div class="col-md-6">
												<div class="form-group"><label for="KdPelanggan">Kode Pelanggan</label><input type="text" id="KdPelanggan" name="kode_pelanggan" placeholder="Kode Pelanggan" class="form-control" value="<?php echo set_value('kode_pelanggan'); ?>" required/></div>
												<div class="form-group"><label for="Email">Email</label>
													<div class="input-group"><span class="input-group-addon"><i class="fa fa-envelope"></i></span><input type="email" id="Email" name="email" placeholder="Email" class="form-control" value="<?php echo set_value('email'); ?>"/></div>
												</div>
											
												<div class="form-group"><label for="Telepon">Telepon</label>
													<div class="input-group"><span class="input-group-addon"><i class="fa fa-phone"></i></span><input type="text" id="Telepon" name="telepon" placeholder="Telepon" class="form-control" value="<?php echo set_value('telepon'); ?>"/></div>
												</div>
												
												<div class="form-group"><label for="Fax">Fax</label>
														<div class="input-group"><span class="input-group-addon"><i class="fa fa-fax"></i></span><input type="text" id="Fax" name="fax" placeholder="Fax" class="form-control" value="<?php echo set_value('fax'); ?>"/></div>
												</div>
										</div>

										<div class="col-md-6">
												<div class="form-group"><label for="NmPelanggan">Nama Pelanggan</label><input type="text" id="NmPelanggan" name="nama_pelanggan" placeholder="Nama Pelanggan" class="form-control" value="<?php echo set_value('nama_pelanggan'); ?>" required/></div>

												<div class="form-group"><label>Status Langganan</label>
													<select class="form-control" id="StsLangganan" name="status_langganan">
														<option value="tetap" <?php echo  set_select('status_langganan', 'tetap'); ?>>Tetap</option>
														<option value="reguler" <?php echo  set_select('status_langganan', 'reguler'); ?>>Reguler</option>
													</select>
												</div>

												<div class="form-group"><label for="InputText1">Alamat</label>
													<input type="text" id="InputText1" name="alamat[]" value="<?php echo set_value('alamat[]'); ?>" placeholder="Kolom 1" class="form-control"/>
													<input type="text" id="InputText2" name="alamat[]" value="<?php echo set_value('alamat[]'); ?>" placeholder="Kolom 2" class="form-control"/>
													<input type="text" id="InputText3" name="alamat[]" value="<?php echo set_value('alamat[]'); ?>" placeholder="Kolom 3" class="form-control"/>
													<input type="text" id="InputText4" name="alamat[]" value="<?php echo set_value('alamat[]'); ?>" placeholder="Kolom 4" class="form-control"/>
													<input type="text" id="InputText5" name="alamat[]" value="<?php echo set_value('alamat[]'); ?>" placeholder="Kolom 5" class="form-control"/>
												</div>
										</div>
								</div>

								<div class="row">
									<div class="col-md-12">
											<div class="form-actions">
												<div class="text-center">
													<button type="submit" class="btn btn-green">Submit</button>
													&nbsp;
													<button type="reset" class="btn btn-default">Cancel</button>
												</div>
											</div>
										</div>
								</div>
						</form>
				</div>
			</div>
		</div>
	</div>
</div>