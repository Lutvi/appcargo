<div class="page-content">
	<div class="row">
		<div class="col-lg-12">
			<div class="portlet box portlet-green">
				<div class="portlet-header">
						<div class="caption">Edit Penerima</div>
				</div>
				<div class="portlet-body">
						<?php echo (validation_errors())?'<div class="alert alert-danger">'.validation_errors().'</div>':''; ?>
						<?php echo form_open('admin/form/up_penerima/'.$id); ?>
								<div class="row">
									<div class="col-md-6">
										<div class="hidden">
											<input type="hidden" name="id_penerima" value="<?php echo $isidata->id_penerima; ?>"/>
											<input type="hidden" name="ori_nama_penerima" value="<?php echo $isidata->nama_penerima; ?>"/>
										</div>
										<div class="form-group"><label for="Cust">Customer</label>
											<select id="Cust" name="customer" class="form-control">
												<option value="">--- Pilih Customer ---</option>
												<?php 
												if(!empty($list_customer)):
													foreach($list_customer as $item): ?>	
												<option value="<?php echo $item->id_customer; ?>" <?php echo  set_select('customer', $item->id_customer, ($isidata->id_customer == $item->id_customer)?true:false); ?>><?php echo $item->kode_pelanggan.' - '.$item->nama_pelanggan; ?></option>
												<?php 
													endforeach;
												endif; ?>
											</select>
										</div>
		
											<div class="form-group"><label for="Kdpos">Kodepos</label>
												<div class="input-group"><span class="input-group-addon"><i class="fa fa-globe"></i></span><input type="text" id="Kdpos" name="kodepos" placeholder="Kodepos" class="form-control" value="<?php echo set_value('kodepos', $isidata->kode_pos); ?>" required/></div>
											</div>

											<div class="form-group"><label for="InputEmail">Email</label>
												<div class="input-group"><span class="input-group-addon"><i class="fa fa-envelope"></i></span><input type="email" id="InputEmail" name="email" placeholder="Email" class="form-control" value="<?php echo set_value('email', $isidata->email); ?>"/></div>
											</div>
										
											<div class="form-group"><label for="Telepon">Telepon</label>
												<div class="input-group"><span class="input-group-addon"><i class="fa fa-phone"></i></span><input type="text" id="Telepon" name="telepon" placeholder="Telepon" class="form-control" value="<?php echo set_value('telepon', $isidata->telepon); ?>"/></div>
											</div>
											
											<div class="form-group"><label for="Fax">Fax</label>
													<div class="input-group"><span class="input-group-addon"><i class="fa fa-fax"></i></span><input type="text" id="Fax"  name="fax" placeholder="Fax" class="form-control" value="<?php echo set_value('fax', $isidata->fax); ?>"/></div>
											</div>
									</div>

									<div class="col-md-6">
										<div class="form-group"><label for="NmPenerima">Nama Penerima</label><input type="text" id="NmPenerima" name="nama_penerima"  placeholder="Nama Penerima" class="form-control" value="<?php echo set_value('nama_penerima', $isidata->nama_penerima); ?>" required/></div>

										<div class="form-group"><label for="Kota">Kota</label>
											<div class="input-group"><span class="input-group-addon"><i class="fa fa-globe"></i></span><input type="text" name="kota"  id="Kota" placeholder="Kota" class="form-control" value="<?php echo set_value('kota', $isidata->kota); ?>" required/></div>
										</div>

										<div class="form-group"><label for="InputText1">Alamat</label>
											<?php 
												$alamat = parsing_alamat($isidata->alamat);
												$jml = count($alamat);
												$i = 1;
												foreach($alamat as $item):
												if($i > 5) continue;
											?>
											<input type="text" id="InputText<?php echo $i; ?>" name="alamat[]" value="<?php echo set_value('alamat[]', $item); ?>" placeholder="Kolom <?php echo $i; ?>" class="form-control"/>
											<?php
												$i++;
												endforeach; ?>
											<?php if($jml <= 5):
												for($x=$i; $x<=5; $x++): ?>
													<input type="text" id="InputText<?php echo $x; ?>" name="alamat[]" value="<?php echo set_value('alamat[]'); ?>" placeholder="Kolom <?php echo $x; ?>" class="form-control"/>
											<?php endfor;
												endif; ?>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
											<div class="form-actions">
												<div class="text-center">
													<button type="submit" class="btn btn-green">Submit</button>
													&nbsp;
													<a href="<?php echo site_url('admin/tabel/penerima'); ?>" class="btn btn-default">Cancel</a>
												</div>
											</div>
										</div>
								</div>
						</form>
				</div>
			</div>
		</div>
	</div>
</div>