<div class="page-content">
	<div class="row">
		<div class="col-lg-12">
			<div class="portlet box portlet-green">
				<div class="portlet-header">
						<div class="caption">Edit Customer</div>
				</div>
				<div class="portlet-body">
						<?php echo (validation_errors())?'<div class="alert alert-danger">'.validation_errors().'</div>':''; ?>
						<?php echo form_open('admin/form/up_customer/'.$id); ?>
								<div class="row">
										<div class="col-md-6">
												<div class="hidden">
													<input type="hidden" name="id_customer" value="<?php echo $isidata->id_customer; ?>"/>
													<input type="hidden" name="ori_nama_pelanggan" value="<?php echo $isidata->nama_pelanggan; ?>"/>
													<input type="hidden" name="ori_kode_pelanggan" value="<?php echo $isidata->kode_pelanggan; ?>"/>
												</div>
												<div class="form-group"><label for="KdPelanggan">Kode Pelanggan</label><input type="text" id="KdPelanggan" name="kode_pelanggan" placeholder="Kode Pelanggan" class="form-control" value="<?php echo set_value('kode_pelanggan', $isidata->kode_pelanggan); ?>"/></div>
												<div class="form-group"><label for="Email">Email</label>
													<div class="input-group"><span class="input-group-addon"><i class="fa fa-envelope"></i></span><input type="email" id="Email" name="email" placeholder="Email" class="form-control" value="<?php echo set_value('email', $isidata->email); ?>"/></div>
												</div>
											
												<div class="form-group"><label for="Telepon">Telepon</label>
													<div class="input-group"><span class="input-group-addon"><i class="fa fa-phone"></i></span><input type="text" id="Telepon" name="telepon" placeholder="Telepon" class="form-control" value="<?php echo set_value('telepon', $isidata->no_telepon); ?>"/></div>
												</div>
												
												<div class="form-group"><label for="Fax">Fax</label>
													<div class="input-group"><span class="input-group-addon"><i class="fa fa-fax"></i></span><input type="text"  id="Fax" name="fax" placeholder="Fax" class="form-control" value="<?php echo set_value('fax', $isidata->fax); ?>"/></div>
												</div>
										</div>

										<div class="col-md-6">
												<div class="form-group"><label for="NmPelanggan">Nama Pelanggan</label><input type="text" id="NmPelanggan" name="nama_pelanggan" placeholder="Nama Pelanggan" class="form-control" value="<?php echo set_value('nama_pelanggan', $isidata->nama_pelanggan); ?>"/></div>

												<div class="form-group"><label>Status Langganan</label>
													<select class="form-control" id="StsLangganan" name="status_langganan">
														<option value="tetap" <?php echo  set_select('status_langganan', 'tetap', ($isidata->status_langganan == 'tetap')?true:false); ?>>Tetap</option>
														<option value="reguler" <?php echo  set_select('status_langganan', 'reguler', ($isidata->status_langganan == 'reguler')?true:false); ?>>Reguler</option>
													</select>
												</div>
												
												<div class="form-group"><label for="InputText1">Alamat</label>
													<?php 
														$alamat = parsing_alamat($isidata->alamat);
														$jml = count($alamat);
														$i = 1;
														foreach($alamat as $item):
														if($i > 5) continue;
													?>
													<input type="text" id="InputText<?php echo $i; ?>" name="alamat[]" value="<?php echo set_value('alamat[]', $item); ?>" placeholder="Kolom <?php echo $i; ?>" class="form-control"/>
													<?php
														$i++;
														endforeach; ?>
													<?php if($jml <= 5):
														for($x=$i; $x<=5; $x++): ?>
															<input type="text" id="InputText<?php echo $x; ?>" name="alamat[]" value="<?php echo set_value('alamat[]'); ?>" placeholder="Kolom <?php echo $x; ?>" class="form-control"/>
													<?php endfor;
														endif; ?>
												</div>
										</div>
								</div>

								<div class="row">
									<div class="col-md-12">
											<div class="form-actions">
												<div class="text-center">
													<button type="submit" class="btn btn-green">Submit</button>
													&nbsp;
													<a href="<?php echo site_url('admin/tabel/customer'); ?>" class="btn btn-default">Cancel</a>
												</div>
											</div>
										</div>
								</div>
						</form>
				</div>
			</div>
		</div>
	</div>
</div>