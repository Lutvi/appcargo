<div class="page-content">
	<div class="row">
		<div class="col-lg-12">
			<div class="portlet box portlet-green">
				<div class="portlet-header">
						<div class="caption">Input Tarif</div>
				</div>
				<div class="portlet-body">
						<?php echo (validation_errors())?'<div class="alert alert-danger">'.validation_errors().'</div>':''; ?>
						<?php echo form_open('admin/form/add_tarif'); ?>
								<div class="row">
										<div class="col-md-6">
											<div class="form-group"><label for="KdTarif">Kode Tarif</label><input type="text" id="KdTarif" name="kode_tarif" placeholder="Kode Tarif" class="form-control" value="<?php echo set_value('kode_tarif'); ?>" required/></div>
											<div class="form-group"><label for="Hg1">Harga Layanan 1</label><input type="text" id="Hg1" name="harga1" placeholder="Harga Layanan 1" class="form-control" value="<?php echo set_value('harga1'); ?>"/></div>
											<div class="form-group"><label for="Hg2">Harga Layanan 2</label><input type="text" id="Hg2" name="harga2" placeholder="Harga Layanan 2" class="form-control" value="<?php echo set_value('harga2'); ?>"/></div>
											<div class="form-group"><label for="Hg3">Harga Layanan 3</label><input type="text" id="Hg3" name="harga3" placeholder="Harga Layanan 3" class="form-control" value="<?php echo set_value('harga3'); ?>"/></div>
										</div>

										<div class="col-md-6">
											<div class="form-group">
												<div class="form-group" style="width:46%;float:left">
													<label for="KdPos">Kodepos</label><input type="text" id="KdPos" name="kodepos" placeholder="Kodepos" class="form-control" value="<?php echo set_value('kodepos'); ?>" required/>
												</div>
												<div class="form-group" style="width:50%;float:right">
													<label for="Services">Service</label>
													<select id="Services" name="service" class="form-control">
														<option value="ONS" <?php echo  set_select('service', 'ONS'); ?>>ONS</option>
														<option value="KILO" <?php echo  set_select('service', 'KILO'); ?>>KILO</option>
													</select>
												</div>
											</div>

											<div class="form-group"><label for="Diskon1">Diskon Layanan 1</label><input type="text" id="Diskon1" name="diskon1" placeholder="Diskon Layanan 1" class="form-control" value="<?php echo set_value('diskon1'); ?>"/></div>
											<div class="form-group"><label for="Diskon2">Diskon Layanan 2</label><input type="text" id="Diskon2" name="diskon2" placeholder="Diskon Layanan 2" class="form-control" value="<?php echo set_value('diskon2'); ?>"/></div>
											<div class="form-group"><label for="Diskon3">Diskon Layanan 3</label><input type="text" id="Diskon3" name="diskon3" placeholder="Diskon Layanan 3" class="form-control" value="<?php echo set_value('diskon3'); ?>"/></div>
										</div>
								</div>

								<div class="row">
									<div class="col-md-12">
											<div class="form-actions">
												<div class="text-center">
													<button type="submit" class="btn btn-green">Submit</button>
													&nbsp;
													<button type="reset" class="btn btn-default">Cancel</button>
												</div>
											</div>
										</div>
								</div>
						</form>
				</div>
			</div>
		</div>
	</div>
</div>