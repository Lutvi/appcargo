		<div class="page-content">
				<div class="row">
						<div class="col-lg-12">
								<div class="portlet box">
										<div class="portlet-header">
												<div class="caption">Customer Listing</div>
												<div class="actions"><a href="<?php echo base_url('admin/form/customer'); ?>" class="btn btn-info btn-xs"><i class="fa fa-plus"></i>&nbsp;
														Input Customer</a>&nbsp;
														<div class="btn-group"><a href="#" data-toggle="dropdown" class="btn btn-warning btn-xs dropdown-toggle"><i class="fa fa-wrench"></i>&nbsp;
																Tools</a>
																<ul class="dropdown-menu pull-right">
																		<li><a href="#">Export to Excel</a></li>
																		<li><a href="#">Export to PDF</a></li>
																		<li class="divider"></li>
																		<li><a href="#">Print HTML</a></li>
																</ul>
														</div>
												</div>
										</div>
										<div class="portlet-body">
												<div class="row mbm">
														<div class="col-lg-4">
																<div class="pagination-panel">Per Page 
																		&nbsp;<select class="form-control input-xsmall input-sm input-inline gw-pageSize">
																				<option value="20" selected="selected">20</option>
																				<option value="50">50</option>
																				<option value="100">100</option>
																				<option value="150">150</option>
																				<option value="-1">All</option>
																		</select>
																</div>
														</div>
														<div class="col-lg-8">			
																<div class="tb-group-actions pull-right"><span>Filter :</span>
																	<input type="text" placeholder="Enter here..." class="table-group-action-select form-control input-inline input-medium input-sm mlm"/>
																	<select class="table-group-action-select form-control input-inline input-small input-sm mlm">
																		<option value="0">Select...</option>
																		<option value="1">Cancel</option>
																		<option value="2">Hold</option>
																		<option value="3">On Hold</option>
																		<option value="4">Close</option>
																	</select>&nbsp;
																		<button class="btn btn-sm btn-green submit-action"><i class="fa fa-check"></i>&nbsp;
																				Submit
																		</button>
																</div>
														</div>
												</div>
												<div class="row mbm">
														<div class="col-lg-12">
																<div class="pagination-panel text-right">
																	<?php echo $this->pagination->create_links(); ?>
																</div>
														</div>
												</div>
												<table class="table table-hover table-striped table-bordered table-advanced">
														<thead>
														<tr>
																<th width="5%">#</th>
																<th width="11%">Kd Pelanggan</th>
																<th>Nama Pelanggan</th>
																<th width="12%">Telepon/Fax</th>
																<th width="12%">Email</th>
																<th width="15%">Alamat</th>
																<th width="9%">Status</th>
																<th width="15%">Actions</th>
														</tr>
														
														</thead>
														<tbody>
												<?php 
													if(!empty($daftar)):
														foreach($daftar as $item): ?>
														<tr>
																<td><?php echo $item->id_customer; ?></td>
																<td><?php echo $item->kode_pelanggan; ?></td>
																<td><?php echo $item->nama_pelanggan; ?></td>
																<td><?php echo $item->no_telepon; ?><br><?php echo $item->fax; ?></td>
																<td><?php echo $item->email; ?></td>
																<td><?php echo parsing_txtalamat($item->alamat); ?></td>
																<td><span class="label label-sm <?php echo ($item->status_langganan == 'tetap')?'label-info':'label-warning'; ?>"><?php echo strtoupper($item->status_langganan); ?></span></td>
																<td>
																		<a href="<?php echo site_url('admin/form/up_customer/'. bskode($item->id_customer)); ?>" class="btn btn-success btn-xs"><i class="fa fa-edit"></i>&nbsp;
																				Edit
																		</a>
																		&nbsp;
																		<button type="button" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i>&nbsp;
																				Delete
																		</button>
																</td>
														</tr>
												<?php 
														endforeach;
													endif; ?>
														</tbody>
												</table>
												<div class="row mbm">
														<div class="col-lg-12">
																<div class="pagination-panel text-center">
																	<?php echo $this->pagination->create_links(); ?>
																</div>
														</div>
												</div>
										</div>
								</div>
						</div>
				</div>
		</div>