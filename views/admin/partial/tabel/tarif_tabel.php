		<div class="page-content">
				<div class="row">
						<div class="col-lg-12">
								<div class="portlet box">
										<div class="portlet-header">
												<div class="caption">Tarif Listing</div>
												<div class="actions"><a href="<?php echo base_url('admin/form/tarif'); ?>" class="btn btn-info btn-xs"><i class="fa fa-plus"></i>&nbsp;
														Input Tarif</a>&nbsp;
														<div class="btn-group"><a href="#" data-toggle="dropdown" class="btn btn-warning btn-xs dropdown-toggle"><i class="fa fa-wrench"></i>&nbsp;
																Tools</a>
																<ul class="dropdown-menu pull-right">
																		<li><a href="#">Export to Excel</a></li>
																		<li><a href="#">Export to PDF</a></li>
																		<li class="divider"></li>
																		<li><a href="#">Print HTML</a></li>
																</ul>
														</div>
												</div>
										</div>
										<div class="portlet-body">
												<div class="row mbm">
														<div class="col-lg-4">
																<div class="pagination-panel">Per Page 
																		&nbsp;<select class="form-control input-xsmall input-sm input-inline gw-pageSize">
																				<option value="20" selected="selected">20</option>
																				<option value="50">50</option>
																				<option value="100">100</option>
																				<option value="150">150</option>
																				<option value="-1">All</option>
																		</select>
																</div>
														</div>
														<div class="col-lg-8">			
																<div class="tb-group-actions pull-right"><span>Filter :</span>
																	<input type="text" placeholder="Enter here..." class="table-group-action-select form-control input-inline input-medium input-sm mlm"/>
																	<select class="table-group-action-select form-control input-inline input-small input-sm mlm">
																		<option value="0">Select...</option>
																		<option value="1">Cancel</option>
																		<option value="2">Hold</option>
																		<option value="3">On Hold</option>
																		<option value="4">Close</option>
																	</select>&nbsp;
																		<button class="btn btn-sm btn-green submit-action"><i class="fa fa-check"></i>&nbsp;
																				Submit
																		</button>
																</div>
														</div>
												</div>
												<div class="row mbm">
														<div class="col-lg-12">
																<div class="pagination-panel text-right">
																	<?php echo $this->pagination->create_links(); ?>
																</div>
														</div>
												</div>
												<table class="table table-hover table-striped table-bordered table-advanced">
														<thead>
														<tr>
																<th width="5%">#</th>
																<th width="15%">Kd Tarif</th>
																<th width="11%">Kodepos</th>
																<th width="12%">Harga Layanan</th>
																<th width="12%">Diskon Layanan</th>
																<th width="9%">Service</th>
																<th width="11%">Actions</th>
														</tr>
														
														</thead>
														<tbody>
												<?php 
													if(!empty($daftar)):
														foreach($daftar as $item): ?>
														<tr>
																<td><?php echo $item->id_tarif; ?></td>
																<td><?php echo $item->kode_tarif; ?></td>
																<td><?php echo $item->kode_pos; ?></td>
																<td class="text-right">
																		<?php echo format_harga_indo($item->harga_layanan_1); ?><br>
																		<?php echo format_harga_indo($item->harga_layanan_2); ?><br>
																		<?php echo format_harga_indo($item->harga_layanan_3); ?>
																</td>
																<td class="text-right">
																		<?php echo $item->diskon_1; ?><br>
																		<?php echo $item->diskon_2; ?><br>
																		<?php echo $item->diskon_3; ?>
																</td>
																<td class="text-center"><span class="label label-sm label-info"><?php echo strtoupper($item->service); ?></span></td>
																<td class="text-center">
																	<a href="<?php echo site_url('admin/form/up_tarif/'. bskode($item->id_tarif)); ?>" class="btn btn-success btn-xs"><i class="fa fa-edit"></i>&nbsp;
																			Edit
																	</a>
																	&nbsp;
																	<button type="button" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i>&nbsp;
																			Delete
																	</button>
																</td>
														</tr>
												<?php 
														endforeach;
													endif; ?>
														</tbody>
												</table>
												<div class="row mbm">
														<div class="col-lg-12">
																<div class="pagination-panel text-center">
																	<?php echo $this->pagination->create_links(); ?>
																</div>
														</div>
												</div>
										</div>
								</div>
						</div>
				</div>
		</div>