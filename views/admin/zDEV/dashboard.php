<?php $this->load->view('admin/element/header'); ?>
<body class="sidebar-colors">
<div>

<!--BEGIN BACK TO TOP--><a id="totop" href="#"><i class="fa fa-angle-up"></i></a><!--END BACK TO TOP-->

	<?php $this->load->view('admin/element/topbar'); ?>
	<?php $this->load->view('admin/element/sidebar'); ?>
	<?php $this->load->view('admin/element/chatform'); ?>

<!--BEGIN PAGE WRAPPER-->
  <div id="page-wrapper">
	<!--BEGIN TITLE & BREADCRUMB PAGE-->
      <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
          <div class="page-title">Dashboard</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
          <li><i class="fa fa-home"></i>&nbsp;<a href="index.html">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
          <li class="hidden"><a href="#">Dashboard</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
          <li class="active">Dashboard</li>
        </ol>
        <div class="clearfix"></div>
      </div>
      <!--END TITLE & BREADCRUMB PAGE-->

<!--BEGIN CONTENT-->
		<?php $this->load->view('admin/partial/'.$content); ?>
<!--END CONTENT-->

		<?php $this->load->view('admin/element/footer'); ?>
	</div>
<!--END PAGE WRAPPER-->

</div>
<?php $this->load->view('admin/element/footer_js'); ?>

</body>
</html>