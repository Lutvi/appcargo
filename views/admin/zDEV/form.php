<?php $this->load->view('admin/element/header_form'); ?>
<body class="sidebar-colors">
<div>
<!--BEGIN BACK TO TOP--><a id="totop" href="#"><i class="fa fa-angle-up"></i></a><!--END BACK TO TOP-->
	<?php $this->load->view('admin/element/topbar'); ?>
	<?php $this->load->view('admin/element/sidebar'); ?>
	<?php $this->load->view('admin/element/chatform'); ?>
<!--BEGIN PAGE WRAPPER-->
	<div id="page-wrapper">
		<!--BEGIN TITLE & BREADCRUMB PAGE-->
		<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
			<div class="page-header pull-left">
					<div class="page-title">Form Components</div>
			</div>
			<ol class="breadcrumb page-breadcrumb pull-right">
					<li><i class="fa fa-home"></i>&nbsp;<a href="index.html">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
					<li><a href="#">Forms</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
					<li class="active">Form Components</li>
			</ol>
			<div class="clearfix"></div>
		</div>
		<!--END TITLE & BREADCRUMB PAGE-->
<!--BEGIN CONTENT-->
		<?php $this->load->view('admin/partial/form/'.$content); ?>
<!--END CONTENT-->
		<?php $this->load->view('admin/element/footer'); ?>
<!--END PAGE WRAPPER-->
		</div>
</div>
<?php $this->load->view('admin/element/footer_form_js'); ?>
</body>
</html>