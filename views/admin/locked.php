<!DOCTYPE html>
<html lang="en">
<head><title>Lock Screen | Extra</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--Loading bootstrap css-->
    <link type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,800italic,400,700,800">
    <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Oswald:400,700,300">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/'); ?>vendors/jquery-ui-1.10.3.custom/css/ui-lightness/jquery-ui-1.10.3.custom.css">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/'); ?>vendors/font-awesome/css/font-awesome.min.css">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/'); ?>vendors/bootstrap/css/bootstrap.min.css">
    <!--Loading style vendors-->
    <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/'); ?>vendors/animate.css/animate.css">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/'); ?>vendors/iCheck/skins/all.css">
    <!--Loading style-->
    <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/'); ?>css/themes/style1/pink-violet.css" id="theme-change" class="style-change color-change">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/'); ?>css/style-responsive.css">
</head>
<body id="lock-screen-page">
<div class="page-form">
    <div class="body-content">
        <div id="lock-screen-avatar"><img src="https://s3.amazonaws.com/uifaces/faces/twitter/kolage/128.jpg" alt="" class="img-responsive img-circle"></div>
        <div id="lock-screen-info"><h1>John Doe</h1>

            <div class="email">example@email.com</div>
            <form action="<?php echo base_url('admin/dashboard'); ?>" class="mtl mbl">
                <div class="input-icon right"><i class="fa fa-unlock"></i><input type="password" value="" class="form-control"></div>
            </form>
        </div>
    </div>
</div>
<script src="<?php echo base_url('assets/'); ?>js/jquery-1.10.2.min.js"></script>
<script src="<?php echo base_url('assets/'); ?>js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?php echo base_url('assets/'); ?>js/jquery-ui.js"></script>
<!--loading bootstrap js-->
<script src="<?php echo base_url('assets/'); ?>vendors/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url('assets/'); ?>vendors/bootstrap-hover-dropdown/bootstrap-hover-dropdown.js"></script>
<script src="<?php echo base_url('assets/'); ?>js/html5shiv.js"></script>
<script src="<?php echo base_url('assets/'); ?>js/respond.min.js"></script>
<script src="<?php echo base_url('assets/'); ?>vendors/iCheck/icheck.min.js"></script>
<script src="<?php echo base_url('assets/'); ?>vendors/iCheck/custom.min.js"></script>
<script src="<?php echo base_url('assets/'); ?>js/app.js"></script>
<script>
var BaseURL = '<?php echo BASE; ?>';
$('input[type="checkbox"]').iCheck({
	checkboxClass: 'icheckbox_minimal-grey',
	increaseArea: '20%' // optional
});
$('input[type="radio"]').iCheck({
	radioClass: 'iradio_minimal-grey',
	increaseArea: '20%' // optional
});
</script>
</body>
</html>