<?php $this->load->view('admin/element/header_form'); ?>
<body class="sidebar-colors">
<div>
<!--BEGIN BACK TO TOP--><a id="totop" href="#"><i class="fa fa-angle-up"></i></a><!--END BACK TO TOP-->
	<?php $this->load->view('admin/element/topbar'); ?>

<!--BEGIN WRAPPER-->
  <div id="wrapper">
	<?php $this->load->view('admin/element/sidebar'); ?>
	<?php //$this->load->view('admin/element/chatform'); ?>

<!--BEGIN PAGE WRAPPER-->
	<div id="page-wrapper">
		<!--BEGIN TITLE & BREADCRUMB PAGE-->
		<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
			<div class="page-header pull-left">
				<div class="page-title"><?php echo $title; ?></div>
			</div>
			<ol class="breadcrumb page-breadcrumb pull-right">
				<li><i class="fa fa-home"></i>&nbsp;<a href="<?php echo base_url('admin/dashboard'); ?>">Dashboard</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
				<li><a href="#">Input Data</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
				<li class="active"><?php echo $title; ?></li>
			</ol>
			<div class="clearfix"></div>
		</div>
		<!--END TITLE & BREADCRUMB PAGE-->
		<!--BEGIN CONTENT-->
		<?php $this->load->view('admin/partial/form/'.$content); ?>
		<!--END CONTENT-->
		<?php //$this->load->view('admin/element/footer'); ?>
		</div>
<!--END PAGE WRAPPER-->

	</div>
<!--END WRAPPER-->
</div>
<?php $this->load->view('admin/element/footer_form_js'); ?>

<?php 
if(isset($content_js)):
	$this->load->view('admin/ajax/'.$content_js);
endif;
?>
</body>
</html>