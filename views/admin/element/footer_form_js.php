<script src="<?php echo base_url('assets/'); ?>js/jquery-1.10.2.min.js"></script>
<script src="<?php echo base_url('assets/'); ?>js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?php echo base_url('assets/'); ?>js/jquery-ui.js"></script>
<!--loading bootstrap js-->
<script src="<?php echo base_url('assets/'); ?>vendors/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url('assets/'); ?>vendors/bootstrap-hover-dropdown/bootstrap-hover-dropdown.js"></script>
<script src="<?php echo base_url('assets/'); ?>js/html5shiv.js"></script>
<script src="<?php echo base_url('assets/'); ?>js/respond.min.js"></script>
<script src="<?php echo base_url('assets/'); ?>vendors/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo base_url('assets/'); ?>vendors/slimScroll/jquery.slimscroll.js"></script>
<script src="<?php echo base_url('assets/'); ?>vendors/jquery-cookie/jquery.cookie.js"></script>
<script src="<?php echo base_url('assets/'); ?>vendors/iCheck/icheck.min.js"></script>
<script src="<?php echo base_url('assets/'); ?>vendors/iCheck/custom.min.js"></script>
<script src="<?php echo base_url('assets/'); ?>vendors/jquery-news-ticker/jquery.news-ticker.js"></script>
<script src="<?php echo base_url('assets/'); ?>js/jquery.menu.js"></script>
<script src="<?php echo base_url('assets/'); ?>vendors/jquery-pace/pace.min.js"></script>
<script src="<?php echo base_url('assets/'); ?>vendors/holder/holder.js"></script>
<script src="<?php echo base_url('assets/'); ?>vendors/responsive-tabs/responsive-tabs.js"></script>
<!--LOADING SCRIPTS FOR PAGE-->
<script src="<?php echo base_url('assets/'); ?>vendors/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url('assets/'); ?>vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="<?php echo base_url('assets/'); ?>vendors/moment/moment.js"></script>
<script src="<?php echo base_url('assets/'); ?>vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<script src="<?php echo base_url('assets/'); ?>vendors/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script src="<?php echo base_url('assets/'); ?>vendors/bootstrap-clockface/js/clockface.js"></script>
<script src="<?php echo base_url('assets/'); ?>vendors/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script src="<?php echo base_url('assets/'); ?>vendors/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<script src="<?php echo base_url('assets/'); ?>vendors/jquery-maskedinput/jquery-maskedinput.js"></script>
<script src="<?php echo base_url('assets/'); ?>js/form-components.js"></script>
<!--CORE JAVASCRIPT-->
<script src="<?php echo base_url('assets/'); ?>js/main.js"></script>
<script>
var BaseURL = '<?php echo BASE; ?>';
</script>