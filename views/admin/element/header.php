<!DOCTYPE html>
<html lang="en">
<head>
	<title>Dashboard</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">

	<link rel="shortcut icon" href="<?php echo base_url('assets/'); ?>images/icons/favicon.ico">
	<link rel="apple-touch-icon" href="<?php echo base_url('assets/'); ?>images/icons/favicon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url('assets/'); ?>images/icons/favicon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url('assets/'); ?>images/icons/favicon-114x114.png">
	<!--Loading bootstrap css-->
	<link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,700">
	<link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Oswald:400,700,300">
	<link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/'); ?>vendors/jquery-ui-1.10.4.custom/css/ui-lightness/jquery-ui-1.10.4.custom.min.css">
	<link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/'); ?>vendors/font-awesome/css/font-awesome.min.css">
	<link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/'); ?>vendors/bootstrap/css/bootstrap.min.css">
	<!--LOADING STYLESHEET FOR PAGE-->
	<link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/'); ?>vendors/intro.js/introjs.css">
	<link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/'); ?>vendors/calendar/zabuto_calendar.min.css">
	<!--Loading style vendors-->
	<link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/'); ?>vendors/animate.css/animate.css">
	<link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/'); ?>vendors/jquery-pace/pace.css">
	<link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/'); ?>vendors/iCheck/skins/all.css">
	<link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/'); ?>vendors/jquery-news-ticker/jquery.news-ticker.css">
	<!--Loading style-->
	<link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/'); ?>css/themes/style1/<?php echo config_item('theme_color'); ?>.css" id="theme-change" class="style-change color-change">
	<link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/'); ?>css/style-responsive.css">
</head>
