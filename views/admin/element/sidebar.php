<!--BEGIN SIDEBAR MENU-->
    <nav id="sidebar" role="navigation" data-step="2" data-intro="Template has &lt;b&gt;many navigation styles&lt;/b&gt;" data-position="right" class="navbar-default navbar-static-side">
      <div class="sidebar-collapse menu-scroll">
        <ul id="side-menu" class="nav">
          <li class="user-panel">
            <div class="thumb"><img src="https://s3.amazonaws.com/uifaces/faces/twitter/kolage/128.jpg" alt="" class="img-circle"/></div>
            <div class="info">
              <p>John Doe</p>
              <ul class="list-inline list-unstyled">
                <li><a href="extra-profile.html" data-hover="tooltip" title="Profile"><i class="fa fa-user"></i></a></li>
                <li><a href="email-inbox.html" data-hover="tooltip" title="Mail"><i class="fa fa-envelope"></i></a></li>
                <li><a href="#" data-hover="tooltip" title="Setting" data-toggle="modal" data-target="#modal-config"><i class="fa fa-cog"></i></a></li>
                <li><a href="extra-signin.html" data-hover="tooltip" title="Logout"><i class="fa fa-sign-out"></i></a></li>
              </ul>
            </div>
            <div class="clearfix"></div>
          </li>
          <li class="<?php echo ($title=='Dashboard')?'active':''; ?>"><a href="<?php echo base_url('admin/dashboard'); ?>"><i class="fa fa-tachometer fa-fw">
            <div class="icon-bg bg-orange"></div>
            </i><span class="menu-title">Dashboard</span></a>
				 </li>
          <li class="<?php echo ($title=='Customer' || $title=='Tarif' || $title=='Transaksi' || $title=='Penerima')?'active':''; ?>"><a href="#"><i class="fa fa-edit fa-fw">
            <div class="icon-bg bg-success"></div>
            </i><span class="menu-title">Input Data</span><span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
              <!--<li><a href="<?php echo base_url('admin/form'); ?>"><i class="fa fa-columns"></i><span class="submenu-title">Contoh Form</span></a></li>-->
              <li class="<?php echo ($title=='Customer')?'active':''; ?>"><a href="<?php echo base_url('admin/form/customer'); ?>"><i class="fa fa-columns"></i><span class="submenu-title">Input Customer</span></a></li>
						<li class="<?php echo ($title=='Penerima')?'active':''; ?>"><a href="<?php echo base_url('admin/form/penerima'); ?>"><i class="fa fa-columns"></i><span class="submenu-title">Input Penerima</span></a></li>
              <li class="<?php echo ($title=='Tarif')?'active':''; ?>"><a href="<?php echo base_url('admin/form/tarif'); ?>"><i class="fa fa-columns"></i><span class="submenu-title">Input Tarif</span></a></li>
              <li class="<?php echo ($title=='Transaksi')?'active':''; ?>"><a href="<?php echo base_url('admin/form/transaksi'); ?>"><i class="fa fa-columns"></i><span class="submenu-title">Input Transaksi</span></a></li>
            </ul>
          </li>
          <li class="<?php echo ($title=='Daftar Customer' || $title=='Daftar Tarif' || $title=='Daftar Transaksi' || $title=='Daftar Penerima')?'active':''; ?>"><a href="#"><i class="fa fa-th-list fa-fw">
            <div class="icon-bg bg-blue"></div>
            </i><span class="menu-title">List Data</span><span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
              <!--<li><a href="<?php echo base_url('admin/tabel'); ?>"><i class="fa fa-th-large"></i><span class="submenu-title">Contoh Tabel</span></a></li>-->
              <li class="<?php echo ($title=='Daftar Customer')?'active':''; ?>"><a href="<?php echo base_url('admin/tabel/customer'); ?>"><i class="fa fa-th-large"></i><span class="submenu-title">Daftar Customer</span></a></li>
						<li class="<?php echo ($title=='Daftar Penerima')?'active':''; ?>"><a href="<?php echo base_url('admin/tabel/penerima'); ?>"><i class="fa fa-th-large"></i><span class="submenu-title">Daftar Penerima</span></a></li>
              <li  class="<?php echo ($title=='Daftar Tarif')?'active':''; ?>"><a href="<?php echo base_url('admin/tabel/tarif'); ?>"><i class="fa fa-th-large"></i><span class="submenu-title">Daftar Tarif</span></a></li>
              <li  class="<?php echo ($title=='Daftar Transaksi')?'active':''; ?>"><a href="<?php echo base_url('admin/tabel/transaksi'); ?>"><i class="fa fa-th-large"></i><span class="submenu-title">Daftar Transaksi</span></a></li>
            </ul>
          </li>
          <li><a href="#"><i class="fa fa-file-o fa-fw">
            <div class="icon-bg bg-yellow"></div>
            </i><span class="menu-title">Laporan</span><span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
              <li><a href="#"><i class="fa fa-puzzle-piece"></i><span class="submenu-title">Laporan Transaksi</span></a></li>
              <li><a href="#"><i class="fa fa-puzzle-piece"></i><span class="submenu-title">Laporan Manifest</span></a></li>
              <li><a href="#"><i class="fa fa-puzzle-piece"></i><span class="submenu-title">Laporan Pendapatan</span></a></li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
<!--END SIDEBAR MENU-->