<?php 

class UserMobile_model extends CI_Model {
	
	public $authMember;
	private $tbl_log_akses = 'tblpro_log_akses_app';
	private $tbl_member = 'tbl_app_member';
	private $tbl_member_auth = 'tbl_app_member_auth';

	/*
		private $tbl_user = 'tbl_user_mobile';
		tbl_user_mobile
	*/

	public function __construct()
	{
		parent::__construct();
		$this->load->library('user_agent');
		// Load Lib authMember
		if(! class_exists('authMember'))
		$this->load->library('authMember');
		$this->authMember = new authMember();
	}
	
	// $tipe : akses1,akses2,akses3
	public function logActivity($tipe='login') {
		$tgl = date('Y-m-d');
		$ipport = userIP();
		$idmem = dikode($this->session->userdata('tokenidmem'), true);
		$nick = $this->session->userdata('nmfull');
		$log = $this->db->get_where($this->tbl_log_akses, array('id_member' => $idmem, 'tipe_akses' => $tipe, 'ip_port' => $ipport, 'tgl_kunjungan' => $tgl));
		
		$data = array('id_member' => $idmem, 'nick_member' => $nick, 
								'tgl_kunjungan' => format_tgl_sql(time()), 'ip_port' => $ipport, 
								'url_asal' => '', 'url_target' => current_url(), 'data_req' => '', 
								'hits' => 1, 'lokasi' => '', 'tipe_akses' => $tipe,
								'browser' => $this->agent->browser(), 'versi' => $this->agent->version(), 'os' => $this->agent->platform(), 'perangkat' => '', 
								'full_platform' => $this->agent->agent_string(), 'full_lokasi' => ''
						);
		
		if(isset($log) && $log->num_rows() > 0) {
			$log = $log->row_array();
			$id_trafik = $log['id_trafik'];
			$data['hits'] = $log['hits']+1;
			$this->db->update($this->tbl_log_akses, $data, array('id_trafik' => $id_trafik));
		}
		else {
			$this->db->insert($this->tbl_log_akses, $data);
		}
	}
	
	public function getMyProfile() {
		$this->authMember->logMemCheck(BASELOGIN);
		$idmem = dikode($this->session->userdata('tokenidmem'), true);
		$profile = $this->db->get_where($this->tbl_member, array('id_mem' => $idmem));
		$data = array();
		if($profile->num_rows() > 0) {
			$row = $profile->row();
			$data = array('nama_lengkap' => dikode($row->nama_lengkap, true), 'email_member' => dikode($row->email_member, true), 'tgl_lahir' => format_tgl_sql($row->tgl_lahir), 'gender' => $row->gender, 'biodata_profil' => dikode($row->biodata_profil, true), 'foto_profil' => $row->foto_profil);
		}
			
		return $data;
	}
	
	public function getMyAuth() {
		$this->authMember->logMemCheck(BASELOGIN);
		$idauth = dikode($this->session->userdata('tokenauth'), true);
		$profile = $this->db->get_where($this->tbl_member_auth, array('id_auth' => $idauth));
		$data = array();
		if($profile->num_rows() > 0) {
			$row = $profile->row();
			$data = array('id_member_login' => dikode($row->id_member_login, true), 'kunci_pas' => dikode($row->kunci_pas, true), 'email_daftar' => dikode($row->email_daftar, true));
		}
		
		return $data;
	}
	
}
