<?php 

class Validasi_model extends CI_Model {

	private $tbl_customer = 'tbl_customer';
	private $tbl_penerima = 'tbl_penerima';
	private $tbl_tarif = 'tbl_tarif';
	private $tbl_transaksi = 'tbl_transaksi';

	public function __construct()
	{
		parent::__construct();		
	}

// Customer
	public function cekKodePelanggan() {
		$cek = $this->db->get_where($this->tbl_customer, array('kode_pelanggan' => $this->input->post('kode_pelanggan')));
		if($cek->num_rows() > 0 )
			return false;
		else
			return true;
	}

	public function cekNamaPelanggan() {
		$cek = $this->db->get_where($this->tbl_customer, array('nama_pelanggan' => $this->input->post('nama_pelanggan')));
		if($cek->num_rows() > 0 )
			return false;
		else
			return true;
	}

	public function cekUpKodePelanggan() {
		$this->db->where('kode_pelanggan', $this->input->post('kode_pelanggan'));
		$this->db->where_not_in('kode_pelanggan', $this->input->post('ori_kode_pelanggan'));
		$cek = $this->db->get($this->tbl_customer);
		if($cek->num_rows() > 0 )
			return false;
		else
			return true;
	}

	public function cekUpNamaPelanggan() {
		$this->db->where('nama_pelanggan', $this->input->post('nama_pelanggan'));
		$this->db->where_not_in('nama_pelanggan', $this->input->post('ori_nama_pelanggan'));
		$cek = $this->db->get($this->tbl_customer);
		if($cek->num_rows() > 0 )
			return false;
		else
			return true;
	}
// End-Customer


// Penerima
	public function cekNamaPenerima() {
		$cek = $this->db->get_where($this->tbl_penerima, array('nama_penerima' => $this->input->post('nama_penerima')));
		if($cek->num_rows() > 0 )
			return false;
		else
			return true;
	}

	public function cekUpNamaPenerima() {
		$this->db->where('nama_penerima', $this->input->post('nama_penerima'));
		$this->db->where_not_in('nama_penerima', $this->input->post('ori_nama_penerima'));
		$cek = $this->db->get($this->tbl_penerima);
		if($cek->num_rows() > 0 )
			return false;
		else
			return true;
	}
// End-Penerima

// Tarif
	public function cekKodeTarif() {
		$cek = $this->db->get_where($this->tbl_tarif, array('kode_tarif' => $this->input->post('kode_tarif')));
		if($cek->num_rows() > 0 )
			return false;
		else
			return true;
	}

	public function cekUpKodeTarif() {
		$this->db->where('kode_tarif', $this->input->post('kode_tarif'));
		$this->db->where_not_in('kode_tarif', $this->input->post('ori_kode_tarif'));
		$cek = $this->db->get($this->tbl_tarif);
		if($cek->num_rows() > 0 )
			return false;
		else
			return true;
	}
// End-Tarif


// Transaksi
	public function cekNoResi() {
		$cek = $this->db->get_where($this->tbl_transaksi, array('no_resi' => $this->input->post('no_resi')));
		if($cek->num_rows() > 0 )
			return false;
		else
			return true;
	}

	public function cekUpNoResi() {
		$this->db->where('no_resi', $this->input->post('no_resi'));
		$this->db->where_not_in('no_resi', $this->input->post('ori_no_resi'));
		$cek = $this->db->get($this->tbl_transaksi);
		if($cek->num_rows() > 0 )
			return false;
		else
			return true;
	}
// End-Transaksi



}