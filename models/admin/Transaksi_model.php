<?php 

class Transaksi_model extends CI_Model {

	private $tbl_transaksi = 'tbl_transaksi';

	public function __construct()
	{
		parent::__construct();
	}

	public function simpanTransaksi(array $data) {
		if(!empty($data))
		{
			return $this->db->insert($this->tbl_transaksi, $data);
		}
		else {
			return false;
		}
	}

	public function updateTransaksi(array $data, $id) {
		if(!empty($data))
		{
			$this->db->where('id_transaksi', bskode($id, true));
			return $this->db->update($this->tbl_transaksi, $data);
		}
		else {
			return false;
		}
	}

	public function getListTransaksi() {
		$this->db->select('id_transaksi,no_resi');
		$this->db->from($this->tbl_customer);
		$query = $this->db->get();

		return $query->result();
	}

	public function countAllTransaksi() {
		return $this->db->count_all($this->tbl_transaksi);
	}

	public function getAllTransaksi(array $filter, $orderby='tgl_transaksi', $dirorder='DESC', $limit=20, $offset=0) {
		$this->db->select('*');
		$this->db->from($this->tbl_transaksi);
		$this->db->order_by($orderby, $dirorder);
		$this->db->limit($limit, $offset);
		$query = $this->db->get();

		return $query->result();
	}

	public function getTransaksiById($id) {
		$query = $this->db->get_where($this->tbl_transaksi, array('id_transaksi' =>  bskode($id, true)), 1);
		
		$row = $query->row();
		if (isset($row))
		return $row;
		else
		return null;
	}

}