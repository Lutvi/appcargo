<?php 

class Tarif_model extends CI_Model {

	private $tbl_tarif = 'tbl_tarif';

	public function __construct()
	{
		parent::__construct();
	}

	public function simpanTarif(array $data) {
		if(!empty($data))
		{
			return $this->db->insert($this->tbl_tarif, $data);
		}
		else {
			return false;
		}
	}

	public function updateTarif(array $data, $id) {
		if(!empty($data))
		{
			$this->db->where('id_tarif', bskode($id, true));
			return $this->db->update($this->tbl_tarif, $data);
		}
		else {
			return false;
		}
	}

	public function getListTarif() {
		$this->db->select('id_tarif,kode_tarif,kode_pos');
		$this->db->from($this->tbl_tarif);
		$query = $this->db->get();

		return $query->result();
	}

	public function countAllTarif() {
		return $this->db->count_all($this->tbl_tarif);
	}

	public function getAllTarif(array $filter, $orderby='kode_pos', $dirorder='ASC', $limit=20, $offset=0) {
		$this->db->select('*');
		$this->db->from($this->tbl_tarif);
		$this->db->order_by($orderby, $dirorder);
		$this->db->limit($limit, $offset);
		$query = $this->db->get();

		return $query->result();
	}

	public function getTarifById($id) {
		$query = $this->db->get_where($this->tbl_tarif, array('id_tarif' =>  bskode($id, true)), 1);
		
		$row = $query->row();
		if (isset($row))
		return $row;
		else
		return null;
	}

}