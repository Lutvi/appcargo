<?php 

class CmsUserMobile_model extends CI_Model {
	
	public $authAdmin;
	private $tbl_log_akses = 'tblpro_log_akses_app';
	private $tbl_member = 'tbl_app_member';
	private $tbl_member_auth = 'tbl_app_member_auth';

	/*
		private $tbl_user = 'tbl_user_mobile';
		tbl_user_mobile
	*/

	public function __construct()
	{
		parent::__construct();
		$this->load->library('user_agent');
		
		// Load Lib authAdmin
		if(! class_exists('authAdmin'))
		$this->load->library('authAdmin');
		$this->authAdmin = new authAdmin();
		$this->authAdmin->logAdmCheck(BASEADMLOGIN);
	}
	
	public function getAllLogActivity($limit=10, $offset=0) {
		$log = $this->db->get($this->tbl_log_akses, $limit, $offset);
		if(isset($log) && $log->num_rows() > 0) {
			return $log->result_array();
		}
		else {
			return null;
		}
	}
	
	public function getLogActivityByTipe($tipe='login', $limit=10, $offset=0) {
		$log = $this->db->get_where($this->tbl_log_akses, array('tipe_akses' => $tipe), $limit, $offset);
		if(isset($log) && $log->num_rows() > 0) {
			return $log->result_array();
		}
		else {
			return null;
		}
	}
	
	// Status : 'on','off','fpass','econf'
	public function getAllMemberAuth($status='*', $limit=10, $offset=0) {
		if($status === '*')
		$profile = $this->db->get($this->tbl_member_auth, $limit, $offset);
		else
		$profile = $this->db->get_where($this->tbl_member_auth, array('status_auth' => $status), $limit, $offset);
	
		$data = array();
		if($profile->num_rows() > 0) {
			$result = $profile->result();
			foreach($result as $row)
			$data[] = array('id_auth' => $row->id_auth, 'id_mem' => $row->id_mem, 'email_daftar' => dikode($row->email_daftar, true), 'tgl_daftar' => $row->tgl_daftar, 'tgl_update_data' => $row->tgl_update_data, 'mem_level' => $row->mem_level, 'status_auth' => $row->status_auth, 'tslog_akhir' => $row->tslog_akhir, 'ttl_login' => $row->ttl_login);
		}
		
		return $data;
	}
	
	public function getMemberAuthById($idauth) {
		$profile = $this->db->get_where($this->tbl_member_auth, array('id_auth' => $idauth));
		$data = array();
		if($profile->num_rows() > 0) {
			$row = $profile->row();
			$data = array('id_member_login' => dikode($row->id_member_login, true), 'kunci_pas' => dikode($row->kunci_pas, true), 'email_daftar' => dikode($row->email_daftar, true));
		}
		
		return $data;
	}
	
}
