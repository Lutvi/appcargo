<?php 

class Customer_model extends CI_Model {

	private $tbl_customer = 'tbl_customer';

	public function __construct()
	{
		parent::__construct();
	}

	public function simpanPelanggan(array $data) {
		if(!empty($data))
		{
			return $this->db->insert($this->tbl_customer, $data);
		}
		else {
			return false;
		}
	}

	public function updatePelanggan(array $data, $id) {
		if(!empty($data))
		{
			$this->db->where('id_customer', bskode($id, true));
			return $this->db->update($this->tbl_customer, $data);
		}
		else {
			return false;
		}
	}

	public function getListPelanggan() {
		$this->db->select('id_customer,kode_pelanggan,nama_pelanggan');
		$this->db->from($this->tbl_customer);
		$query = $this->db->get();

		return $query->result();
	}

	public function countAllPelanggan() {
		return $this->db->count_all($this->tbl_customer);
	}

	public function getAllPelanggan(array $filter, $orderby='nama_pelanggan', $dirorder='ASC', $limit=20, $offset=0) {
		$this->db->select('*');
		$this->db->from($this->tbl_customer);
		$this->db->order_by($orderby, $dirorder);
		$this->db->limit($limit, $offset);
		$query = $this->db->get();

		return $query->result();
	}

	public function getPelangganById($id) {
		$query = $this->db->get_where($this->tbl_customer, array('id_customer' =>  bskode($id, true)), 1);
		
		$row = $query->row();
		if (isset($row))
		return $row;
		else
		return null;
	}

}