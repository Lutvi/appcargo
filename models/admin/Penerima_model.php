<?php 

class Penerima_model extends CI_Model {

	private $tbl_penerima = 'tbl_penerima';

	public function __construct()
	{
		parent::__construct();
	}

	public function simpanPenerima(array $data) {
		if(!empty($data))
		{
			return $this->db->insert($this->tbl_penerima, $data);
		}
		else {
			return false;
		}
	}

	public function updatePenerima(array $data, $id) {
		if(!empty($data))
		{
			$this->db->where('id_penerima', bskode($id, true));
			return $this->db->update($this->tbl_penerima, $data);
		}
		else {
			return false;
		}
	}

	public function getListPenerima() {
		$this->db->select('id_penerima,nama_penerima,kode_pos');
		$this->db->from($this->tbl_penerima);
		$query = $this->db->get();

		return $query->result();
	}

	public function countAllPenerima() {
		return $this->db->count_all($this->tbl_penerima);
	}

	public function getAllPenerima(array $filter, $orderby='nama_penerima', $dirorder='ASC', $limit=20, $offset=0) {
		$this->db->select('*');
		$this->db->from($this->tbl_penerima);
		$this->db->order_by($orderby, $dirorder);
		$this->db->limit($limit, $offset);
		$query = $this->db->get();

		return $query->result();
	}

	public function getPenerimaById($id) {
		$query = $this->db->get_where($this->tbl_penerima, array('id_penerima' =>  bskode($id, true)), 1);
		
		$row = $query->row();
		if (isset($row))
		return $row;
		else
		return null;
	}

}