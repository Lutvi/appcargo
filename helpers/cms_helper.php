<?php 
/*
 * name: cms_helper.php
 * untuk manipulasi dan format mail-template/sitemap-xml/text/string
 * 08-03-2016  Lutvi  <fun9uy5@gmail.com>
 */

function convertToObject($array) {
	$object = new stdClass();
	foreach ($array as $key => $value) {
		if (is_array($value)) {
			$value = convertToObject($value);
		}
		$object->$key = $value;
	}
	return $object;
}

function convertToArray($object) {
	if( !is_object( $object ) && !is_array( $object ) )
	{
		return $object;
	}
	if( is_object( $object ) )
	{
		$object = get_object_vars( $object );
	}
	return array_map( 'convertToArray', $object );
}

//-- List All Method from class
function objectToArray($obj) { 
	$arr = array();
	$_arr = is_object($obj) ? get_object_vars($obj) : $obj;
	foreach ($_arr as $key => $val) {
		$val = (is_array($val) || is_object($val)) ? object_to_array($val) : $val;
		$arr[$key] = $val;
	}
	return $arr;
}

function array_slice_assoc($array,$keys) {
    return array_intersect_key($array,array_flip($keys));
}

function userIP() {
	$ip = '127.0.0.1';
	if(getenv('HTTP_X_FORWARDED_FOR')){
		$ip = getenv('HTTP_X_FORWARDED_FOR');
	}
	elseif (getenv('HTTP_CLIENT_IP')){
		$ip = getenv('HTTP_CLIENT_IP');
	}
	elseif(getenv('REMOTE_ADDR')) {
		$ip = getenv('REMOTE_ADDR');
	} 
	else {
		$ip = $_SERVER['REMOTE_ADDR']; 
	}
	return $ip;
}
 
// format kirim email user
function format_isi_email_update_profil($nama='',$usrid='',$pass='',$email='') {
	if(BHS == 'id')
	{
		$isi  = 'Kepada : '.$nama.'<br>'."\n";
		$isi .= 'Telah terjadi perubahan pada data akun Anda, pada : '.date('d/m/Y h:i A').'<br>'."\n";
		$isi .= 'Kami telah menperbaharui data akun Anda dalam sistem kami.<br>'."\n";
		$isi .= 'Berikut detail login untuk akun Anda.<br>'."\n";
		$isi .= 'Nama Lengkap : '.$nama.'<br>'."\n";
		$isi .= 'Akun ID      : '.$usrid.'<br>'."\n";
		//$isi .= 'Kata Sandi   : '.$pass.'<br>'."\n";
		$isi .= 'Email        : '.$email.'<br>'."\n";
		$isi .= '<hr>Ini hanyalah email notifikasi untuk perubahan data Akun Anda.<br>'."\n";
		$isi .= '<p>'.NAMA_WEB.'.</p>';
	}
	else {
		$isi  = 'To : '.$nama.'<br>'."\n";
		$isi .= 'There has been a change on your account, on : '.date('Y/m/d h:i A').'<br>'."\n";
		$isi .= 'We have updated your account in our system.<br>'."\n";
		$isi .= 'Here\'s the login details for your account.<br>'."\n";
		$isi .= 'Full Name    : '.$nama.'<br>'."\n";
		$isi .= 'Account ID   : '.$usrid.'<br>'."\n";
		//$isi .= 'Passwords    : '.$pass.'<br>'."\n";
		$isi .= 'Email        : '.$email.'<br>'."\n";
		$isi .= '<hr>This is just a notification email for your account data changes.<br>'."\n";
		$isi .= '<p>'.NAMA_WEB.'.</p>';
	}

	return $isi;
}

function format_isi_email_registrasi($nama='',$usrid='',$pass='',$email='') {
	if(BHS == 'id')
	{
		$isi  = 'Kepada : '.$nama.'<br>'."\n";
		$isi .= 'Terima kasih telah melakukan registrasi pada website kami.<br>'."\n";
		$isi .= 'Kami telah mendaftarkan Akun untuk Anda dalam sistem kami.<br>'."\n";
		$isi .= 'Berikut detail login untuk akun Anda.<br>'."\n";
		$isi .= 'Akun ID    : '.$usrid.'<br>'."\n";
		$isi .= 'Kata Sandi : '.$pass.'<br>'."\n";
		$isi .= 'Email      : '.$email.'<br>'."\n";
		$isi .= '<hr>Mohon simpan baik-baik Akun Anda, Anda bisa merubah detail login password melalui profil akun Anda.<br>'."\n";
		$isi .= '<p>Hormat kami,<br>'.NAMA_WEB.'.</p>';
	}
	else {
		$isi  = 'To : '.$nama.'<br>'."\n";
		$isi .= 'Thank you for signup on our website.<br>'."\n";
		$isi .= 'We have registered your account in our system.<br>'."\n";
		$isi .= 'Here\'s the login details for your account.<br>'."\n";
		$isi .= 'Account ID : '.$usrid.'<br>'."\n";
		$isi .= 'Passwords  : '.$pass.'<br>'."\n";
		$isi .= 'Email      : '.$email.'<br>'."\n";
		$isi .= '<hr>Please keep your account secure, to be safe you can change the account password through your account profile.<br>'."\n";
		$isi .= '<p>Sincerely,<br>'.NAMA_WEB.'.</p>';
	}

	return $isi;
}

/* MIcs */
function cek_koneksi_internet($timeout=1, $url='') {
	$status = TRUE;

	if(STATUS == 'development')
	{
		if (empty($url))
		$conn = @fsockopen("php.net", 80, $errno, $errstr, $timeout);
		else
		$conn = @fsockopen($url, 80, $errno, $errstr, $timeout);
		
		if ($conn)
		{
			fclose($conn);
		}
		else
		{
			$status = FALSE;
		}
	}
	
	return $status; 
}

function iniajax() {
	return ( ! empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest');
}

function refresh($url='', $time=0) {
	if ( ! headers_sent())
	header("Refresh:$time; url=$url");
	else {
		echo "<meta http-equiv=\"refresh\" content=\"$time;url=$url\">";
		exit();
	}
}

function tsout($in='',$format=TRUE) {
	if($format)
	print('DATA : <small><pre>'.print_r($in, TRUE).'</pre></small>');
	else
	var_dump($in);
}

//-- upgrade valid email
function ini_valid_email($email) {
	if (preg_match('/^([\w\.\%\+\-]+)@([a-z0-9\.\-]+\.[a-z]{2,6})$/i',trim($email),$m)) 
	{
		if ( (checkdnsrr($m[2],'MX') == TRUE) || (checkdnsrr($m[2],'A') == TRUE) ) 
		{
			return TRUE;
		}
		else {
			return FALSE;
		}
	}
	else {
		return FALSE;
	}
}

// Judul-String NON htmlspecialchar
function jdstring($string) {
	$string = rip_tags($string);
	$string = filter_var($string, FILTER_SANITIZE_STRING);
	return $string;
}

// Format judul for edit/show-html
function inptxtjudul($str) { 
	return htmlspecialchars_decode($str, ENT_NOQUOTES);
}

function inptxtcari($str) { 
	$str = filter_var($str, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
	$str = filter_var($str, FILTER_SANITIZE_URL);
	$str = html_entity_decode(inptxtjudul($str));
	// Special Char allow("-", "_")
	$xvalA = array("'", "\\", "/", "<", ">", "~", "`", "&", "?", "\"","!","@","#","$","%","^","*","(",")","+","=",":",";");
	// Entt Encoded
	$xvalB = array("#34;","#39;");
	$xval = array_merge($xvalA, $xvalB);
	$str = str_replace($xval, "", $str);
	$str = html_entity_decode(inptxtjudul($str));
	return $str;
}

function urljudul($str='') {
	$str = str_replace('#39', '', $str);
	$str = rip_tags($str);	
	$str = str_replace('.', '', $str);
	$str = str_replace(' ', '-', strtolower($str));
	
	return $str;
}
// DECODE urljudul jd normal text
function jdjudul($str='') {
	$str = str_replace('-', ' ', $str);
	$str = ucwords($str);
	return $str;
}
// Limit text
function limit_words($string='', $word_limit=20) {
	$string = preg_replace('/\W\w+\s*(\W*)$/', '$1', $string);
	$words = explode(" ",$string);
	return implode(" ",array_splice($words,0,$word_limit));
}
// Mysql -> html-txt(text editor)-> masih harus replace lg di view
function jdhtml($html='') {
	$html = html_entity_decode(html_entity_decode($html));
	$html = preg_replace("#&nbsp;+#", " ", $html);
	return $html;
}
// Front-End
function viewhtml($html='') {
	$html = jdhtml(html_entity_decode($html));
	$html = preg_replace("#&nbsp;+#", " ", $html);
	return $html;
}
// html -> text
function html2txt($document) {
	$search = array('@<script[^>]*?>.*?</script>@si','@<[\/\!]*?[^<>]*?>@si','@<style[^>]*?>.*?</style>@siU','@<![\s\S]*?--[ \t\n\r]*>@'); 
	$text = preg_replace($search, '', viewhtml($document)); 
	return $text; 
}

// format bersihkan spasi yg double
function bersih_spasi($str='')
{
	return preg_replace('/\s\s+/', ' ', $str);
}
// trim - awal-akhir
function trim_all($str) {
	$trim = preg_replace('#\s+#', ' ', $str);
	return ucwords(trim($trim));
}
// bersihkan tag-html
function rip_tags($string='') {
	$string = jdhtml($string);
	$string = strip_tags($string);
	$string = urldecode($string);
	$string = preg_replace('/[^A-Za-z0-9]/', ' ', $string);
	$string = preg_replace('/ +/', ' ', $string);
	$string = trim($string);
	$string = preg_replace ('/<[^>]*>/', ' ', $string);
	$string = str_replace("\r", '', $string);
	$string = str_replace("\n", ' ', $string);
	$string = str_replace("\t", ' ', $string);

	return $string;
}

function format_komastring2json($str='') {
	$kategori = explode(",", $kategori);
	return json_encode($kategori);
}

function format_json2array($tag) {
	$ftag = json_decode($tag, true);
	return $ftag;
}


// format sort tgl jadi ts
function format_ts2tgl($tgl='') {
	if($tgl != '')
	return date('Y-m-d H:i:s', strtotime($tgl));
}

// format tanggal
function format_tgl($bhs='id', $tgl='', $full=false) {
	// Convert int
	if(is_string($tgl))
	$tgl = strtotime($tgl);
	else
	$tgl = intval($tgl);
	
	if($bhs == 'id') {
		if(!$full)
		$ftgl = date('d-m-Y', $tgl);
		else
		$ftgl = date('d-m-Y H:i:s', $tgl);
	}
	else {
		if(!$full)
		$ftgl = date('Y-m-d', $tgl);
		else
		$ftgl = date('Y-m-d H:i:s', $tgl);
	}
	
	return $ftgl;
}

function format_tgl_indo($tgl='') {
	if(is_string($tgl))
	return date('d-m-Y H:i:s', strtotime($tgl));
	else
	return date('d-m-Y H:i:s', intval($tgl));
}


function format_tgl_indo_jq($tgl='') {
	if(is_string($tgl))
	return date('d-m-Y', strtotime($tgl));
	else
	return date('d-m-Y', intval($tgl));
}

function format_tgl_jq($tgl='') {
	if(is_string($tgl))
	return date('Y-m-d', strtotime($tgl));
	else
	return date('Y-m-d', intval($tgl));
}

// Tgl TS
function format_tglwaktu_ts($tgl='', $setts=false) {
	$outts = 0;
	if(is_string($tgl) && strlen($tgl) == 10) {
		$outtgl = $tgl.' '.date('H:i:s');
		$outts = strtotime($outtgl);
	}
	else {
		$outtgl = $tgl;
		$outts = strtotime($outtgl);
	}
	
	if($setts)
	return $outts;
	else
	return $outtgl;
}

// Tgl-SQL
function format_ts_sql($tgl='') {
	if($tgl != '0000-00-00 00:00:00')
		return format_tgl_jq($tgl);
	else
		return '';
}

function format_tgl_sql($tgl='') {
	if(is_string($tgl))
	return date('Y-m-d H:i:s', strtotime($tgl));
	else
	return date('Y-m-d H:i:s', intval($tgl));
}

// Format Tgl(id,en)
function formatTanggal($bhs='id', $date=null, $full=false) {
	$array_hari = array(1=>'Senin','Selasa','Rabu','Kamis','Jumat', 'Sabtu','Minggu');
	$array_bulan = array(1=>'Januari','Februari','Maret', 'April', 'Mei', 'Juni','Juli','Agustus',
	'September','Oktober', 'November','Desember');
	$waktu = '';
	
	if($full)
		$waktu = date('H:i:s');
	
	if($date == null) {
		$hari = $array_hari[date('N')];
		$tanggal = date ('j');
		$bulan = $array_bulan[date('n')];
		$tahun = date('Y');
	} else {
		if(is_string($date))
		$date = strtotime($date);
		else
		$date = intval($date);
	
		$hari = $array_hari[date('N',$date)];
		$tanggal = date ('j', $date);
		$bulan = $array_bulan[date('n',$date)];
		$tahun = date('Y',$date);
	}
	$formatTanggal = $hari . ", " . $tanggal .".". $bulan .".". $tahun;

	if($bhs == 'id')
	return $formatTanggal.' '.$waktu;
	else
	return date('l, Y.m.d').' '.$waktu;
}

//== EXTRA:
function format_string_aman($str='') {
	$txt = filter_var($str, FILTER_SANITIZE_STRING);
	return $txt;
}

function get_http_response_code($domain) {
	if(isset($_SERVER['REDIRECT_STATUS']))
		return $_SERVER['REDIRECT_STATUS'];
	else {
		$headers = get_headers($domain);
		return substr($headers[0], 9, 3);
	}
}

function get_extfile($filename) {
	$path_info = @pathinfo($filename);
	return $path_info['extension'];
}

// Simple sendmail
function sortcut_sendemail($to,$subject,$message,$from) {
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=UTF-8'. "\r\n";
	$headers .= 'From: '.$from .' Reply-To: '.$from.'\r\n X-Mailer: PHP/'. phpversion();
	if(@mail($to,$subject,$message,$headers)){
		return true;
	} else {
		return false;
	}
}
// Gen Random-Pass
function randomPassword($len) {
	$key=null;
	$pattern="XYZ01A23B45C67D89EabFcdGefHghIijkJlmKonLpqrsMtyNvwOxyPzQRSTUVW";
	for($i=0;$i<$len;$i++){
		$key.=$pattern{rand(0,61)};
	}
	return $key;
}
// Gen Rand-Key
function generateKey() {
	$seed=(double) microtime() * 1000000;
	srand($seed);
	return rand();
}

// Format Alamat
function format_alamat($alamat) {
	$out = '';
	if(is_array($alamat) && !empty($alamat)) {
		foreach($alamat as $item)
			$out .= $item.'##';
	}

	return $out;
}

function parsing_alamat($alamat) {
	$out = array();
	if($alamat != '') {
		$alamat = explode('##', $alamat);
		foreach($alamat as $item)
			$out[] = $item;
	}

	return $out;
}

function parsing_txtalamat($alamat) {
	$out = "";
	if($alamat != '') {
		$alamat = explode('##', $alamat);
		foreach($alamat as $item)
			$out .= $item."\r\n<br>";
	}

	return $out;
}


// No-seri
function genseri($num=0, $len=4) {
	return str_pad($num, $len, "0", STR_PAD_LEFT);
}

// No-Struk
function gen_nostruk($noseri='',$kdpos='',$prx1='TR',$prx2='A',$prx3='C') {
	$noseri = genseri($noseri);
	return $prx1.date('ym').$prx2.$noseri.$prx3.$kdpos;
}

// Time Ago
function timeago($ts='', $singkat=false) {
	if($ts != '') {
		if(!is_numeric($ts))
			$ts = strtotime($ts);
		
		// Hitung selisih
		$selisih = time() - $ts;
		
		if($selisih < 1) {
			if(BHS === 'id')
			return 'baru saja';
			else
			return 'just now';
		}
			
		if(BHS === 'id') {
			$cekond = array( 
				12 * 30 * 24 * 60 * 60  =>  ($singkat)?'thn':'tahun',
				30 * 24 * 60 * 60       =>  ($singkat)?'bln':'bulan',
				24 * 60 * 60            =>  ($singkat)?'d':'hari',
				60 * 60                 =>  ($singkat)?'h':'jam',
				60                      =>  ($singkat)?'m':'menit',
				1                       =>  ($singkat)?'s':'detik'
			);
		}
		else {
			$cekond = array( 
				12 * 30 * 24 * 60 * 60  =>  ($singkat)?'yrs':'year',
				30 * 24 * 60 * 60       =>  ($singkat)?'mth':'month',
				24 * 60 * 60            =>  ($singkat)?'d':'day',
				60 * 60                 =>  ($singkat)?'h':'hour',
				60                      =>  ($singkat)?'m':'minute',
				1                       =>  ($singkat)?'s':'second'
			);
		}

		// Rangkai kalimat
		foreach( $cekond as $detik => $str ) {
			$d = $selisih / $detik;
			
			if( $d >= 1 ) {
				$r = round( $d );
				
				if($singkat) {
					return $r . $str;
				}
				else {
					if(BHS === 'id')
						return 'sekitar ' . $r . ' ' . $str . ' yg lalu';
					else 
						return 'about ' . $r . ' ' . $str . ( $r > 1 ? 's' : '' ) . ' ago';
				}
			}
		}
	}
}

// format no-hp
function format_nohp($phone='', $lokal='+62') {
	$phone = preg_replace("/[^0-9]/", "", $phone);

	if(strlen($phone) == 7)
		return preg_replace("/([0-9]{3})([0-9]{4})/", "$1-$2", $phone);
	elseif(strlen($phone) == 10)
		return preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "($1) $2-$3", $phone);
	elseif(strlen($phone) >= 12)
		return preg_replace("/([0-9]{1})([0-9]{3})([0-9]{4})([0-9]{4})/", "$lokal$2-$3-$4", $phone);
	else
		return $phone;
}

// format rupiah
function format_harga_indo($nilai='') {
	return number_format($nilai, 0,'','.');
}

// manipulasi tanggal
function cek_selisih_tgl($tgl_awal, $tgl_akhir, $tgl_kini='') {
	// Convert to timestamp
	$awal_ts = strtotime($tgl_awal);
	$akhir_ts = strtotime($tgl_akhir);
	if($tgl_kini != '')
	$kini_ts = strtotime($tgl_kini);
	else
	$kini_ts = time();

	// bool
	return (($kini_ts >= $awal_ts) && ($kini_ts <= $akhir_ts));
}

// cek waktu sampai
function waktu_sampai($tsSisaWaktu=0, $akhirWaktu=null) { 
	if(!empty($akhirWaktu)) {
		if(is_string($akhirWaktu))
		$tsSisaWaktu = strtotime(format_ts2tgl($akhirWaktu)) - time();
		else
		$tsSisaWaktu = (int)$akhirWaktu - time();
	}

	if($tsSisaWaktu > 0) {
		$hari = floor($tsSisaWaktu / 86400);
		$tsSisaWaktu = $tsSisaWaktu - $hari * 86400;
		$jam = floor($tsSisaWaktu / 3600);
		$tsSisaWaktu = $tsSisaWaktu - $jam * 3600;
		$mnt = floor($tsSisaWaktu / 60);
		$dtk = $tsSisaWaktu - $mnt * 60;
	} 
	else {
		return array(0, 0, 0, 0); 
	}

	return array($hari, $jam, $mnt, $dtk);
}

// format waktu(array) jd text
function waktu2text($waktu) {
	
	if(BHS === 'id')
	$bhs = array('hari','jam','menit','detik');
	else
	$bhs = array('days','hours','minutes','seconds');

	$txtout = '';
	if(!empty($waktu) && is_array($waktu)) {
		$i = 0;
		foreach($waktu as $isi) {
			if(isset($waktu[$i]) && $waktu[$i] > 0)
				$txtout .= $waktu[$i].$bhs[$i].'  ';
			$i++;
		}
		
		//if($txtout == '')
		//$txtout = (BHS === 'id')?$waktu[3].'hari ini':'today';
	}

	return $txtout;
}

function remove_empty($array) {
  return array_filter($array, '_remove_empty_internal');
}

function _remove_empty_internal($value) {
  return !empty($value) || $value === 0;
}
