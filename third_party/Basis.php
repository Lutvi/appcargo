<?php 
/*
	Project encryption class
	sort encryption funct
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if (version_compare(PHP_VERSION, '5.4.0', '<') ) exit("Sorry, this CORE will only run on PHP version 5.4.0 or greater!<br>\r\nPlease contact Author for details.\r\n");

class Basis
{
    private $iv = 'go2webmodrnz2017';
    private $key = '3n91nersz';

    function enkripKode($str='',$hash='') {
      $iv = $this->iv;
      $td = mcrypt_module_open('rijndael-128', '', 'cbc', $iv);

      mcrypt_generic_init($td, $this->key, $iv);
      $encrypted = mcrypt_generic($td, $str);

      mcrypt_generic_deinit($td);
      mcrypt_module_close($td);

      return bin2hex($encrypted);
    }

    function dekripKode($code='') {
      $code = $this->hex2bin($code);
      $iv = $this->iv;

      $td = mcrypt_module_open('rijndael-128', '', 'cbc', $iv);

      mcrypt_generic_init($td, $this->key, $iv);
      $decrypted = mdecrypt_generic($td, $code);

      mcrypt_generic_deinit($td);
      mcrypt_module_close($td);

      return utf8_encode(trim($decrypted));
    }

    protected function hex2bin($hexdata) {
      $bindata = '';

      for ($i = 0; $i < strlen($hexdata); $i += 2) {
            $bindata .= chr(hexdec(substr($hexdata, $i, 2)));
      }

      return $bindata;
    }
}

class Anti_Mcrypt
{
    const KeyMinLength = 10;
    const KeyMaxLength = 32; // mcrypt demand
    const SaltPad = '#89767bgxdrfp4^%KBRVJH$#%I ^:LM() {NV CQX-][":.7n36F;-';

    private $_td;
    private $_cypher = MCRYPT_RIJNDAEL_128;
    private $_mode   = MCRYPT_MODE_CBC;
    private $_key = null;

    public function __construct($key)
    {
        if (!extension_loaded("mcrypt")) {
            throw new Anti_Mcrypt_Exception("mcrypt php extension not found but required");
        }
        $this->_td = mcrypt_module_open($this->_cypher, '', $this->_mode, '');
        $this->_setKey($key);
    }

    public function encrypt($plaintext)
    {
        if (is_null($this->_key)) {
            throw new Anti_Mcrypt_Exception("no key set");
        }
        $iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($this->_td), MCRYPT_RAND);
        mcrypt_generic_init($this->_td, $this->_key, $iv);
        $crypttext = mcrypt_generic($this->_td, $plaintext);
        mcrypt_generic_deinit($this->_td);
        return base64_encode($iv . $crypttext);
    }

    public function decrypt($crypttext)
    {
        if (is_null($this->_key)) {
            throw new Anti_Mcrypt_Exception("no key set");
        }
        $crypttext = base64_decode($crypttext);
        $ivsize = mcrypt_get_iv_size($this->_cypher, $this->_mode);
        $iv = substr($crypttext, 0, $ivsize);
        $crypttext = substr($crypttext, $ivsize);
        mcrypt_generic_init($this->_td, $this->_key, $iv);
        $plaintext = mdecrypt_generic($this->_td, $crypttext);
        mcrypt_generic_deinit($this->_td);
        return $plaintext;
    }

    public function __destruct()
    {
        mcrypt_module_close($this->_td);
    }

    private function _setKey($key)
    {
        if (strlen($key) < self::KeyMinLength) {
            throw new Anti_Mcrypt_Exception("the key is too short, expected at minimum of %s, given %s", 0, array(self::KeyMinLength, strlen($key)));
        } else if (strlen($key) > self::KeyMaxLength) {
            throw new Anti_Mcrypt_Exception("the key is too long, expected a maximum of %s, given %s", 0, array(self::KeyMaxLength, strlen($key)));
        }
        $this->_key = substr($key . self::SaltPad, 0, self::KeyMaxLength);
    }
}

function quote($kat) 
{
	return sprintf("'%s'", $kat);
}

if ( ! function_exists('bskode')) //Static
{
	function bskode($str='', $bongkar=FALSE)
	{
		if(empty($str))
		return '';

		if ( ! class_exists('Basis')) exit('Basis Class not installed, Please contact Author!');
		$bs = new Basis();
		if($bongkar === TRUE)
		{
			return trim($bs->dekripKode($str));
		} else {
			return $bs->enkripKode($str);
		}
	}
}

if ( ! function_exists('ackode')) //Dinamis-Variasi
{
	function ackode($str='', $bongkar=FALSE, $key=0, $sep="__")
	{
		if(empty($str))
		return '';

		if ( ! class_exists('Basis')) exit('Basis Class not installed, Please contact Author!');
		if($bongkar === TRUE)
		{
			$nstr = bskode($str,true);
			$nstr = explode('__',$nstr);
			if(count($nstr)==2) {
				return $nstr[$key];
			}
			else
			return '';
		} else {
			if($key==0)
			$nstr = $str.$sep.time();
			else
			$nstr = time().$sep.$str;
		
			return bskode($nstr);
		}
	}
}

if ( ! function_exists('dikode'))
{
	function dikode($str='', $bongkar=FALSE)
	{
		if(empty($str))
		return '';

		if ( ! class_exists('Anti_Mcrypt')) exit('Anti_Mcrypt class not installed, Please contact Author!');
		$cr = new Anti_Mcrypt("go2webmodrnz2017");
		if($bongkar === TRUE)
		{
			return trim($cr->decrypt($str));
		} else {
			return $cr->encrypt($str);
		}
	}
}
