<?php 

$config['full_tag_open'] = '<ul class="pagination mtm mbm">';
$config['full_tag_close'] = '</ul>';
$config['first_link'] = '&lsaquo;&lsaquo;';
$config['first_tag_open'] = '<li>';
$config['first_tag_close'] = '</li>';
$config['last_link'] = '&rsaquo;&rsaquo;';
$config['last_tag_open'] = '<li>';
$config['last_tag_close'] = '</li>';
$config['num_links'] = 5;
$config['next_link'] = '&rsaquo;';
$config['next_tag_open'] = '<li>';
$config['next_tag_close'] = '</li>';
$config['prev_link'] = '&lsaquo;';
$config['prev_tag_open'] = '<li>';
$config['prev_tag_close'] = '</li>';
$config['num_tag_open'] = '<li>';
$config['num_tag_close'] = '</li>';
$config['cur_tag_open'] = '<li class="active"><a href="#">';
$config['cur_tag_close'] = '</a></li>';

$config['use_page_numbers'] = TRUE;