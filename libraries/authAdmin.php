<?php
/*===================================
authAdmin class
-----------
author : lutvi(fun9uy5@gmail.com)
package: appcargo
required helper : cms_helper
required third_party : Basis
=====================================*/

require_once APPPATH . '/third_party/Basis.php';

class authAdmin {
	
	protected $CI;
	private $tbl_admin_auth = 'tblpro_sys_login_admin';
	
	function __construct() {
		$this->CI =& get_instance();
	}
	
	// -- Forgot Password
	function buildForgetUrlAdm($url, $idmem, $token) {
		$cek = $this->CI->db->query("SELECT * FROM " . $this->tbl_admin_auth . " WHERE ilog=$idmem AND status='fpass' LIMIT 1 ");
		$row = $cek->row_array();
		if (isset($row)) {
			return $token;
		}
		else {
			$timeout = time()+3600;
			$this->CI->db->update($this->tbl_admin_auth, array('status'=>'fpass', 'token'=>$token, 'token_timeout'=>$timeout), array('ilog' => $idmem));
			return $url . bskode($idmem) . '__' . bskode($token);
		}
	}
	
	function cekForgetUrlAdm($econf) {
		$econf = filter_var($econf, FILTER_SANITIZE_STRING);
		$econf = explode('__',$econf);
		if(count($econf)==2) {
			$now = time();
			$idmem = bskode($econf[0], true);
			$token = bskode($econf[1], true);
			if(is_numeric($idmem)) {
				$sql = "SELECT * FROM ".$this->tbl_admin_auth." WHERE ilog = $idmem AND token = '$token' AND status = 'fpass' LIMIT 1 ";
				$cek = $this->CI->db->query($sql);
				if($cek->num_rows() > 0) {
					foreach($cek->result_array() as $row) {
						$timeout = $row['token_timeout'];
						if($timeout > $now) {
							$npass = dikode(trim($token));
							$this->CI->db->update($this->tbl_admin_auth, array('kunci_pas'=>$npass, 'status'=>'on', 'token'=>'', 'token_timeout'=>0), array('ilog' => $idmem));
							return true;
						}
						else {
							return 'timeout';
						}
					}
					
				}
				else {
					return '404';
				}
			}
			else {
				return 'errF';
			}
		}
		return 'errF';
	}
	// -- End Forgot Password
	
	// -- Konfirmasi Regis
	function buildConfLinkAdm($url, $lastid, $token) {
		$cek = $this->CI->db->query("SELECT * FROM " . $this->tbl_admin_auth . " WHERE ilog=$lastid AND status='econf' ");
		$row = $cek->row_array();
		if (isset($row)) {
			$token = $row['token'];
		}
		return $url . bskode($lastid) . '__' . bskode($token);
	}
	
	function cekKonfirmasiAdm($econf) {
		$econf = filter_var($econf, FILTER_SANITIZE_STRING);
		$econf = explode('__', $econf);
		if(count($econf) == 2) {
			$now = time();
			$ilog = bskode($econf[0], true);
			$token = bskode($econf[1], true);
			if(is_numeric($ilog)) {
				$sql = "SELECT * FROM " . $this->tbl_admin_auth . " WHERE ilog = $ilog AND token = '$token' AND status = 'econf' LIMIT 1";
				$cek = $this->CI->db->query($sql);
				$row = $cek->row_array();
				if (isset($row)) {
					foreach($row as $item) {
						$timeout = $item['token_timeout'];
						if($timeout > $now) {
							$this->CI->db->update($this->tbl_admin_auth, array('status'=>'on', 'token'=>'', 'token_timeout'=>0), array('ilog' => $idmem));
							return true;
						}
						else {
							$this->CI->db->delete($this->tbl_admin_auth, array('ilog' => $ilog));
							return 'timeout';
						}
					}
				}
				else {
					return '404';
				}
			}
			else {
				return 'errF';
			}
		}
		return 'errF';
	}
	// -- End Konfirmasi Regis	
	
	function cekAvailableAdm($userid, $email) {
		$cek = true;
		$userid = filter_var($userid, FILTER_SANITIZE_STRING);
		$email = filter_var($email, FILTER_SANITIZE_STRING);
		// Cek db
		$cntlogin = $this->CI->db->count_all($this->tbl_admin_auth);
		$login = $this->CI->db->get($this->tbl_admin_auth);
		if($cntlogin > 0) {
			foreach($login->result_array() as $row) {
				$match1 = dikode($row['id_masuk'], true);
				$match2 = dikode($row['email_admin'], true);
				if($userid == $match1 || $email == $match2) {
					if($userid == $match1) {
						// userid sudah terdaftar
						$cek = 'error1'; 
						continue;
					}
					elseif($email == $match2) {
						// email sudah terdaftar
						$cek = 'error2';
						continue;
					}
				}
			}
		}
		return $cek;
	}
	
	function addAdmin($dtmember=array()) {
		$cek = $this->cekAvailableAdm($dtmember['idmasuk'], $dtmember['email']);
		if($cek === true) {
			$data_auth = array(
						'id_masuk' => dikode($dtmember['idmasuk']),
						'email_admin' => dikode($dtmember['email']),
						'kunci_masuk' => dikode($dtmember['kunci']),
						'nama_admin' => dikode($dtmember['nama']),
						'level' => (isset($dtmember['level']))?$dtmember['level']:'admin', 
						'status' => (isset($dtmember['status']))?$dtmember['status']:'econf'
					);
			
			if(isset($dtmember['token']))
			$data_auth = array_merge($data_auth, array('token'=>$dtmember['token'], 'token_timeout'=>time()+3600));
		
			if($this->CI->db->insert($this->tbl_admin_auth, $data_auth)) {
				$mem_id =  $this->CI->db->insert_id();
				$data_auth['ilog'] = $mem_id;
				return $mem_id;
			}
			else
			return false;
		}
		else
			return $cek;
	}
	
	// Form Login
	function cekAdmLogin($userid, $kunci, $json_lstakses='') { 
		$cek = '404';
		$now = time();
		$userid = filter_var($userid, FILTER_SANITIZE_STRING);
		$kunci = filter_var($kunci, FILTER_SANITIZE_STRING);
		// Cek db
		$cntlogin = $this->CI->db->count_all($this->tbl_admin_auth);
		$login = $this->CI->db->get($this->tbl_admin_auth);
		if($cntlogin > 0) {
		foreach($login->result_array() as $row) {
			$match1 = dikode($row['id_masuk'],true);
			$match2 = dikode($row['kunci_masuk'],true);
			if($userid === $match1 && $kunci === $match2)
			{
				if($row['status'] == 'on' || $row['status'] == 'fpass') {
					$cek = true;
					$idmem = $row['ilog'];
					// Get Member Info
					$lsesi_mem = array();
					$mem_info = $this->CI->db->get_where($this->tbl_admin_auth, array('ilog' => $idmem));
					$mem = $mem_info->row_array();
					$lsesi_auth = array('admin' => trim($row['id_masuk']), 'atoken' => trim($row['kunci_masuk']), 
								 'mailadm' => trim($row['email_admin']), 'lvadm' => dikode($row['level']), 
								 'tokenadm' => dikode($row['id_masuk']),
								 'tokenidadm' => dikode($idmem));
					if(isset($mem)) {
						$lsesi_mem = array('nmadm' => dikode($mem['nama_admin'], true), 'admfoto' => dikode($mem['foto'], true), 'bioadm' => strtotime($mem['bio_data']));
					}
					$lsesi = array_merge($lsesi_auth, $lsesi_mem); 
					$this->CI->session->set_userdata($lsesi);
					$data = array('id_masuk' => dikode($userid), 'kunci_masuk' => dikode($kunci), 'log_akhir' => format_tgl_sql(time()), 'ttl_login'=>$row['ttl_login']+1);
					// konfirmasi forget password via email -- DIBATALKAN
					if($row['status'] == 'fpass') { 
						$data['status'] = 'on';
						$data['token'] = '';
						$data['token_timeout'] = 0;
					}
					$this->CI->db->update($this->tbl_admin_auth, $data, array('ilog' => $row['ilog']));
					//$this->CI->session->set_flashdata('sukses', 'au_sukses');
					break;
				}
				elseif($row['status'] == 'off') { 
					// banned-status:off
					$cek = 'banned';	
					break;
				}
				// konfirmasi token via email
				elseif($row['status'] == 'econf') { 
					$cek = '403';
					if($row['token_timeout'] >= $now) {
						$idmem = $row['ilog'];
						$this->CI->db->delete($this->tbl_admin_auth, array('ilog' => $idmem));
						$cek = '404';
					}					
					break;
				}
				else {
					$cek = false; 
					break;
				}
			}
		}
		}
		return $cek;
	}
	
	// Param $level = sesi('lvmem')
	function cekAdmAuth($level, $allow=array('super', 'admin', 'asisten')) {
		if($level!='' && $this->CI->session->userdata('admin') != '' && $this->CI->session->userdata('tokenadm') != '' && $this->CI->session->userdata('tokenidadm') != '') {
			$tokenid = dikode($this->CI->session->userdata('tokenidadm'), true);
			$level = dikode($level, true);
			if(in_array($level, $allow) && is_numeric($tokenid))
				return true;
			else 
				return false;
		}
		else {
			return false;
		}
	}
	
	// Cek sesi akses
	function logAdmCheck($url='', $ajax=false, $isadm=false) { 
		if($url=='')
		$url = BASELOGIN;
		// jqXHR.responseText === "logout-ajax";
		$ajax = iniajax();
		
		if(!$isadm && $this->CI->session->userdata('valogadm') != '' && $this->CI->session->userdata('valogadm') === 'ok') {
			if($this->CI->session->userdata('admin') != '' && $this->CI->session->userdata('tokenadm') != '' && $this->CI->session->userdata('tokenidadm') != '' && $this->CI->session->userdata('mailadm') != '') {
				$idauth = filter_var(trim($this->CI->session->userdata('tokenidadm')), FILTER_SANITIZE_STRING);
				$idmem = filter_var(trim($this->CI->session->userdata('tokenadm')), FILTER_SANITIZE_STRING);
				$tokena = (int)dikode($idauth, true); 
				$tokenb = (int)dikode($idmem, true);
				$cntlogin = $this->CI->db->count_all($this->tbl_admin_auth, " ilog=$tokena AND id_masuk=$tokenb ");
				if($cntlogin === 1)
					return true;
				else {
					if(!$ajax)
					$this->logOutAdm('au_gagal1b', $url);
					else
					exit('logout-ajax');
				}
			}
			else {
				if(!$ajax)
				$this->logOutAdm('au_gagal1b', $url);
				else
				exit('logout-ajax');
			}
		}
		else {
			if($this->CI->session->userdata('admin') != '' && $this->CI->session->userdata('tokenadm') != '' && $this->CI->session->userdata('tokenidadm') != '' && $this->CI->session->userdata('mailadm') != '') {
				$idx = filter_var(trim($this->CI->session->userdata('tokenidadm')), FILTER_SANITIZE_STRING);
				$uid = filter_var(trim($this->CI->session->userdata('tokenadm')), FILTER_SANITIZE_STRING);
				$token = (int)dikode($idx, true);
				$userid = (int)dikode($uid, true);
				if(is_numeric($token)) {
					$sql = "SELECT * FROM " . $this->tbl_admin_auth . " WHERE ilog = $token LIMIT 1";
					$cek = $this->CI->db->query($sql);
					$row = $cek->row_array();
					if (isset($row)) {
						//foreach($cek as $row) {
							$id_masuk = (int)dikode($row['id_masuk'], true);
							if($id_masuk == $userid) {
								if($row['status']=='off') {
									if(!$ajax)
									$this->logOutAdm('au_gagal1b', $url);
									else
									exit('logout-ajax');
								}
								else {
									$this->CI->session->set_userdata('valog', 'ok');
									return true;
								}							
							}
							else {
								if(!$ajax)
								$this->logOutAdm('au_gagal1a', $url);
								else
								exit('logout-ajax');
							}
						//}
					}
					else {
						if(!$ajax)
						$this->logOutAdm('au_gagal2', $url);
						else
						exit('logout-ajax');
					}
				}
				else {
					if(!$ajax)
					$this->logOutAdm('au_gagal3', $url);
					else
					exit('logout-ajax');
				}
			}
			else {
				if(!$ajax)
				$this->logOutAdm('au_gagal4', $url);
				else
				exit('logout-ajax');
			}
		}
	}
	
	function logOutAdm($msg='', $url='') {
		// off log
		$ladm = array('admin','mailadm','mailmem','lvadm','tokenadm','tokenidadm','nmadm','admfoto','bioadm','valogadm');
		$this->CI->session->unset_userdata($ladm);
		if($msg != '')
			$this->CI->session->set_flashdata('warning', $msg);
		if($url=='')
		$url = BASELOGIN;
		if($url != '') {
			redirect($url);
		}
	}
	
	function rmeAdmInsert() {
		$token = trim($this->CI->session->userdata('atoken'));
		$token = dikode($token);
		setcookie('nx', $token, time() + (86400 * 7), '/');
		setcookie('zx', trim($this->CI->session->userdata('admin')), time() + (86400 * 7), '/');
	}
	
	function rmeAdmDel() {
		setcookie('nx', '', time() - 3600, '/');
	}
	
	function rmeMemDelAll() {
		setcookie('nx', '', time() - 3600, '/');
		setcookie('zx', '', time() - 3600, '/');
	}
	
	function rmeAdmCheck() {
		if(isset($_COOKIE['nx']) && $_COOKIE['nx'] != '' && isset($_COOKIE['zx']) && $_COOKIE['zx'] != '') {
			$idx = trim($_COOKIE['nx']);
			$idx = filter_var($idx, FILTER_SANITIZE_STRING);
			$uid = trim($_COOKIE['zx']);
			$uid = filter_var($uid, FILTER_SANITIZE_STRING);
			$token = dikode($idx, true);
			$token = dikode($token, true);
			// 2x enkrip
			$userid = dikode($uid, true); 
			if(is_numeric($token)) {
				$sql = "SELECT * FROM " . $this->tbl_admin_auth . " WHERE ilog = $token LIMIT 1";
				$cek = $this->CI->db->query($sql);
				$row = $cek->row_array();
				if (isset($row)) {
					//foreach($cek as $row) {
						$id_masuk = dikode($row['id_member_login'], true);
						if($id_masuk == $userid)
							return true;
						else {
							$this->rmeAdmDel();
							return false;
						}
					//}
				}
				else {
					$this->rmeAdmDel();
					return false;
				}
			}
			else {
				$this->rmeAdmDel();
				return false;
			}
		}
		else {
			return false;
		}
	}
}