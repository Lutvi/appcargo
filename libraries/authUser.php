<?php
/*===================================
authUser class
-----------
author : lutvi(fun9uy5@gmail.com)
package: appcargo
required helper : cms_helper
required third_party : Basis
=====================================*/

require_once APPPATH . '/third_party/Basis.php';

class authUser {
	
	protected $CI;
	private $tbl_user_mobile = 'tbl_user_mobile';
	
	function __construct() {
		$this->CI =& get_instance();
	}

/*	
	// -- Forgot Password
	function buildForgetUrlMem($url, $idmem, $token) {
		$cek = $this->CI->db->query("SELECT * FROM " . $this->tbl_user_mobile . " WHERE username=$idmem AND status_auth='fpass' LIMIT 1 ");
		$row = $cek->row_array();
		if (isset($row)) {
			return $token;
		}
		else {
			$timeout = time()+3600;
			$this->CI->db->update($this->tbl_user_mobile, array('status_auth'=>'fpass', 'token'=>$token, 'token_timeout'=>$timeout), array('username' => $idmem));
			return $url . bskode($idmem) . '__' . bskode($token);
		}
	}
	
	function cekForgetUrlMem($econf) {
		$econf = filter_var($econf, FILTER_SANITIZE_STRING);
		$econf = explode('__',$econf);
		if(count($econf)==2) {
			$now = time();
			$idmem = bskode($econf[0], true);
			$token = bskode($econf[1], true);
			if(is_numeric($idmem)) {
				$sql = "SELECT * FROM ".$this->tbl_user_mobile." WHERE username = $idmem AND token = '$token' AND status_auth = 'fpass' LIMIT 1 ";
				$cek = $this->CI->db->query($sql);
				if($cek->num_rows() > 0) {
					foreach($cek->result_array() as $row) {
						$timeout = $row['token_timeout'];
						if($timeout > $now) {
							$npass = dikode(trim($token));
							$this->CI->db->update($this->tbl_user_mobile, array('password'=>$npass, 'status_auth'=>'on', 'token'=>'', 'token_timeout'=>0), array('username' => $idmem));
							return true;
						}
						else {
							return 'timeout';
						}
					}
					
				}
				else {
					return '404';
				}
			}
			else {
				return 'errF';
			}
		}
		return 'errF';
	}
	// -- End Forgot Password
	
	// -- Konfirmasi Regis
	function buildConfLinkMem($url, $lastid, $token) {
		$cek = $this->CI->db->query("SELECT * FROM " . $this->tbl_user_mobile . " WHERE username=$lastid AND status_auth='econf' ");
		$row = $cek->row_array();
		if (isset($row)) {
			$token = $row['token'];
		}
		return $url . bskode($lastid) . '__' . bskode($token);
	}
	
	function cekKonfirmasiMem($econf) {
		$econf = filter_var($econf, FILTER_SANITIZE_STRING);
		$econf = explode('__', $econf);
		if(count($econf) == 2) {
			$now = time();
			$username = bskode($econf[0], true);
			$token = bskode($econf[1], true);
			if(is_numeric($username)) {
				$sql = "SELECT * FROM " . $this->tbl_user_mobile . " WHERE username = $username AND token = '$token' AND status_auth = 'econf' LIMIT 1";
				$cek = $this->CI->db->query($sql);
				$row = $cek->row_array();
				if (isset($row)) {
					foreach($row as $item) {
						$timeout = $item['token_timeout'];
						if($timeout > $now) {
							$this->CI->db->update($this->tbl_user_mobile, array('status_auth'=>'on', 'token'=>'', 'token_timeout'=>0), array('username' => $idmem));
							return true;
						}
						else {
							$this->CI->db->delete($this->tbl_user_mobile, array('username' => $username));
							return 'timeout';
						}
					}
				}
				else {
					return '404';
				}
			}
			else {
				return 'errF';
			}
		}
		return 'errF';
	}
	// -- End Konfirmasi Regis	
*/
	
	function cekAvailableMem($userid, $nama) {
		$cek = true;
		$userid = filter_var($userid, FILTER_SANITIZE_STRING);
		$nama = filter_var($nama, FILTER_SANITIZE_STRING);
		// Cek db
		$cntlogin = $this->CI->db->count_all($this->tbl_user_mobile);
		$login = $this->CI->db->get($this->tbl_user_mobile);
		if($cntlogin > 0) {
			foreach($login->result_array() as $row) {
				$match1 = dikode($row['username'], true);
				$match2 = dikode($row['nama'], true);
				if($userid == $match1 || $nama == $match2) {
					if($userid == $match1) {
						// userid sudah terdaftar
						$cek = 'error1'; 
						continue;
					}
					elseif($nama == $match2) {
						// nama sudah terdaftar
						$cek = 'error2'; 
						continue;
					}
				}
			}
		}
		return $cek;
	}
	
	function addMember($dtmember=array()) {
		$cek = $this->cekAvailableMem($dtmember['username'], $dtmember['nama']);
		if($cek === true) {
			$data_auth = array(
						'nama' => dikode($dtmember['nama']),
						'username' => dikode($dtmember['username']),
						'password' => dikode($dtmember['password']),
						'wilayah' => dikode($dtmember['wilayah']),
						'level' => dikode($dtmember['level']),
						'kode_pos' => dikode($dtmember['kode_pos'])
					);
		
			if($this->CI->db->insert($this->tbl_user_mobile, $data_auth)) {
				$mem_id =  $this->CI->db->insert_id();
				return $mem_id;
			}
			else
			return false;
		}
		else
			return $cek;
	}
	
	// Form Login
	function cekMemLogin($userid, $kunci, $json_lstakses='') { 
		$cek = '404';
		$now = time();
		$userid = filter_var($userid, FILTER_SANITIZE_STRING);
		$kunci = filter_var($kunci, FILTER_SANITIZE_STRING);
		// Cek db
		$cntlogin = $this->CI->db->count_all($this->tbl_user_mobile);
		$login = $this->CI->db->get($this->tbl_user_mobile);
		if($cntlogin > 0) {
			foreach($login->result_array() as $row) {
				$match1 = dikode($row['username'],true);
				$match2 = dikode($row['password'],true);
				if($userid === $match1 && $kunci === $match2)
				{
					$cek = true;
					$idmem = $row['id_user_mobile'];
					// Get Member Info
					$lsesi_mem = array();
					$mem_info = $this->CI->db->get_where($this->tbl_user_mobile, array('id_user_mobile' => $idmem));
					$mem = $mem_info->row_array();
					$lsesi_auth = array('member' => trim($row['username']),
								 'tokenauth' => dikode($row['id_user_mobile']),
								 'tokenidmem' => dikode($idmem));
					if(isset($mem)) {
						$lsesi_mem = array('nmmem' => dikode($mem['nama'], true));
					}
					$lsesi = array_merge($lsesi_auth, $lsesi_mem); 
					$this->CI->session->set_userdata($lsesi);
					
					break;
				}
			}
		}
		return $cek;
	}
	
	// Param $level = sesi('lvmem')
	function cekMemAuth($level, $allow=array('free', 'premium', 'gold')) {
		if($level!='' && $this->CI->session->userdata('member') != '' && $this->CI->session->userdata('tokenauth') != '' && $this->CI->session->userdata('tokenidmem') != '') {
			$tokenid = dikode($this->CI->session->userdata('tokenauth'), true);
			$level = dikode($level, true);
			if(in_array($level, $allow) && is_numeric($tokenid))
				return true;
			else 
				return false;
		}
		else {
			return false;
		}
	}
	
	// Cek sesi akses
	function logMemCheck($url='', $ajax=false, $isadm=false) { 
		if($url=='')
		$url = BASELOGIN;
		// jqXHR.responseText === "logout-ajax";
		$ajax = iniajax();
		
		if(!$isadm && $this->CI->session->userdata('valogmem') != '' && $this->CI->session->userdata('valogmem') === 'ok') {
			if($this->CI->session->userdata('member') != '' && $this->CI->session->userdata('tokenauth') != '' && $this->CI->session->userdata('tokenidmem') != '') {
				$idauth = filter_var(trim($this->CI->session->userdata('tokenauth')), FILTER_SANITIZE_STRING);
				$idmem = filter_var(trim($this->CI->session->userdata('tokenidmem')), FILTER_SANITIZE_STRING);
				$tokena = (int)dikode($idauth, true); 
				$tokenb = (int)dikode($idmem, true);
				$cntlogin = $this->CI->db->count_all($this->tbl_user_mobile, " id_user_mobile=$tokena AND username=$tokenb ");
				if($cntlogin === 1)
					return true;
				else {
					if(!$ajax)
					$this->logOutMem('au_gagal1b', $url);
					else
					exit('logout-ajax');
				}
			}
			else {
				if(!$ajax)
				$this->logOutMem('au_gagal1b', $url);
				else
				exit('logout-ajax');
			}
		}
		else {
			if($this->CI->session->userdata('member') != '' && $this->CI->session->userdata('tokenauth') != '' && $this->CI->session->userdata('tokenidmem') != '') {
				$idx = filter_var(trim($this->CI->session->userdata('tokenidmem')), FILTER_SANITIZE_STRING);
				$uid = filter_var(trim($this->CI->session->userdata('member')), FILTER_SANITIZE_STRING);
				$token = (int)dikode($idx, true);
				$userid = dikode($uid, true);
				if(is_numeric($token)) {
					$sql = "SELECT * FROM " . $this->tbl_user_mobile . " WHERE id_user_mobile = $token LIMIT 1";
					$cek = $this->CI->db->query($sql);
					$row = $cek->row_array();
					if (isset($row)) {
						//foreach($cek as $row) {
							$id_masuk = dikode($row['username'], true);
							if($id_masuk == $userid) {
								/*if($row['status_auth']=='off') {
									if(!$ajax)
									$this->logOutMem('au_gagal1b', $url);
									else
									exit('logout-ajax');
								}
								else {*/
									$this->CI->session->set_userdata('valog', 'ok');
									return true;
								//}
							}
							else {
								if(!$ajax)
								$this->logOutMem('au_gagal1a', $url);
								else
								exit('logout-ajax');
							}
						//}
					}
					else {
						if(!$ajax)
						$this->logOutMem('au_gagal2', $url);
						else
						exit('logout-ajax');
					}
				}
				else {
					if(!$ajax)
					$this->logOutMem('au_gagal3', $url);
					else
					exit('logout-ajax');
				}
			}
			else {
				if(!$ajax)
				$this->logOutMem('au_gagal4', $url);
				else
				exit('logout-ajax');
			}
		}
	}
	
	function logOutMem($msg='', $url='') {
		// off log
		$ladm = array('nmmem','member','tokenauth','tokenidmem','valogmem');
		$this->CI->session->unset_userdata($ladm);
		if($msg != '')
			$this->CI->session->set_flashdata('warning', $msg);
		if($url=='')
		$url = BASELOGIN;
		if($url != '') {
			redirect($url);
		}
	}
	
	function rmeMemInsert() {
		$token = trim($this->CI->session->userdata('tokenidmem'));
		$token = dikode($token);
		setcookie('nxm1', $token, time() + (86400 * 7), '/');
		setcookie('zxm1', trim($this->CI->session->userdata('member')), time() + (86400 * 7), '/');
	}
	
	function rmeMemDel() {
		setcookie('nxm1', '', time() - 3600, '/');
	}
	
	function rmeMemDelAll() {
		setcookie('nxm1', '', time() - 3600, '/');
		setcookie('zxm1', '', time() - 3600, '/');
	}
	
	function rmeMemCheck() {
		if(isset($_COOKIE['nxm1']) && $_COOKIE['nxm1'] != '' && isset($_COOKIE['zxm1']) && $_COOKIE['zxm1'] != '') {
			$idx = trim($_COOKIE['nxm1']);
			$idx = filter_var($idx, FILTER_SANITIZE_STRING);
			$uid = trim($_COOKIE['zxm1']);
			$uid = filter_var($uid, FILTER_SANITIZE_STRING);
			$token = dikode($idx, true);
			$token = dikode($token, true);
			// 2x enkrip
			$userid = dikode($uid, true); 
			if(is_numeric($token)) {
				$sql = "SELECT * FROM " . $this->tbl_user_mobile . " WHERE username = $token LIMIT 1";
				$cek = $this->CI->db->query($sql);
				$row = $cek->row_array();
				if (isset($row)) {
					//foreach($cek as $row) {
						$id_masuk = dikode($row['username'], true);
						if($id_masuk == $userid)
							return true;
						else {
							$this->rmeMemDel();
							return false;
						}
					//}
				}
				else {
					$this->rmeMemDel();
					return false;
				}
			}
			else {
				$this->rmeMemDel();
				return false;
			}
		}
		else {
			return false;
		}
	}
}