<?php
/*===================================
authMember class
-----------
author : lutvi(fun9uy5@gmail.com)
package: appcargo
required helper : cms_helper
required third_party : Basis
=====================================*/

require_once APPPATH . '/third_party/Basis.php';

class authMember {
	
	protected $CI;
	private $tbl_member = 'tbl_app_member';
	private $tbl_member_auth = 'tbl_app_member_auth';
	
	function __construct() {
		$this->CI =& get_instance();
	}
	
	// -- Forgot Password
	function buildForgetUrlMem($url, $idmem, $token) {
		$cek = $this->CI->db->query("SELECT * FROM " . $this->tbl_member_auth . " WHERE id_mem=$idmem AND status_auth='fpass' LIMIT 1 ");
		$row = $cek->row_array();
		if (isset($row)) {
			return $token;
		}
		else {
			$timeout = time()+3600;
			$this->CI->db->update($this->tbl_member_auth, array('status_auth'=>'fpass', 'token'=>$token, 'token_timeout'=>$timeout), array('id_mem' => $idmem));
			return $url . bskode($idmem) . '__' . bskode($token);
		}
	}
	
	function cekForgetUrlMem($econf) {
		$econf = filter_var($econf, FILTER_SANITIZE_STRING);
		$econf = explode('__',$econf);
		if(count($econf)==2) {
			$now = time();
			$idmem = bskode($econf[0], true);
			$token = bskode($econf[1], true);
			if(is_numeric($idmem)) {
				$sql = "SELECT * FROM ".$this->tbl_member_auth." WHERE id_mem = $idmem AND token = '$token' AND status_auth = 'fpass' LIMIT 1 ";
				$cek = $this->CI->db->query($sql);
				if($cek->num_rows() > 0) {
					foreach($cek->result_array() as $row) {
						$timeout = $row['token_timeout'];
						if($timeout > $now) {
							$npass = dikode(trim($token));
							$this->CI->db->update($this->tbl_member_auth, array('kunci_pas'=>$npass, 'status_auth'=>'on', 'token'=>'', 'token_timeout'=>0), array('id_mem' => $idmem));
							return true;
						}
						else {
							return 'timeout';
						}
					}
					
				}
				else {
					return '404';
				}
			}
			else {
				return 'errF';
			}
		}
		return 'errF';
	}
	// -- End Forgot Password
	
	// -- Konfirmasi Regis
	function buildConfLinkMem($url, $lastid, $token) {
		$cek = $this->CI->db->query("SELECT * FROM " . $this->tbl_member_auth . " WHERE id_mem=$lastid AND status_auth='econf' ");
		$row = $cek->row_array();
		if (isset($row)) {
			$token = $row['token'];
		}
		return $url . bskode($lastid) . '__' . bskode($token);
	}
	
	function cekKonfirmasiMem($econf) {
		$econf = filter_var($econf, FILTER_SANITIZE_STRING);
		$econf = explode('__', $econf);
		if(count($econf) == 2) {
			$now = time();
			$id_mem = bskode($econf[0], true);
			$token = bskode($econf[1], true);
			if(is_numeric($id_mem)) {
				$sql = "SELECT * FROM " . $this->tbl_member_auth . " WHERE id_mem = $id_mem AND token = '$token' AND status_auth = 'econf' LIMIT 1";
				$cek = $this->CI->db->query($sql);
				$row = $cek->row_array();
				if (isset($row)) {
					foreach($row as $item) {
						$timeout = $item['token_timeout'];
						if($timeout > $now) {
							$this->CI->db->update($this->tbl_member_auth, array('status_auth'=>'on', 'token'=>'', 'token_timeout'=>0), array('id_mem' => $idmem));
							return true;
						}
						else {
							$this->CI->db->delete($this->tbl_member_auth, array('id_mem' => $id_mem));
							return 'timeout';
						}
					}
				}
				else {
					return '404';
				}
			}
			else {
				return 'errF';
			}
		}
		return 'errF';
	}
	// -- End Konfirmasi Regis	
	
	function cekAvailableMem($userid, $email) {
		$cek = true;
		$userid = filter_var($userid, FILTER_SANITIZE_STRING);
		$email = filter_var($email, FILTER_SANITIZE_STRING);
		// Cek db
		$cntlogin = $this->CI->db->count_all($this->tbl_member_auth);
		$login = $this->CI->db->get($this->tbl_member_auth);
		if($cntlogin > 0) {
			foreach($login->result_array() as $row) {
				$match1 = dikode($row['id_member_login'], true);
				$match2 = dikode($row['email_daftar'], true);
				if($userid == $match1 || $email == $match2) {
					if($userid == $match1) {
						// userid sudah terdaftar
						$cek = 'error1'; 
						continue;
					}
					elseif($email == $match2) {
						// email sudah terdaftar
						$cek = 'error2'; 
						continue;
					}
				}
			}
		}
		return $cek;
	}
	
	function addMember($dtmember=array()) {
		$cek = $this->cekAvailableMem($dtmember['idmem'], $dtmember['email']);
		if($cek === true) {
			$data_auth = array(
						'tgl_daftar' => format_tgl_sql(time()),
						'email_daftar' => dikode($dtmember['email']),
						'id_member_login' => dikode($dtmember['idmem']),
						'kunci_pas' => dikode($dtmember['kunci']),
						//'mem_level' => (isset($dtmember['level']))?$dtmember['level']:'free', 
						'status_auth' => (isset($dtmember['status']))?$dtmember['status']:'econf'
					);
			// level ('free','premium','gold') : free
			// status ('on','off','fpass','econf') : econf
			$data_member = array(
						'nama_panggilan' => dikode($dtmember['nick']),
						'nama_lengkap' => dikode($dtmember['nama']),
						'email_member' => dikode($dtmember['email']),
						'tgl_lahir' => format_tgl_jq($dtmember['tgl_lahir']),
						'gender' => $dtmember['gender'],
						//'biodata_profil' => dikode($dtmember['biodata']),
						//'foto_profil' => dikode($dtmember['foto']),
					);
			// prioritas_cust ('biasa','normal','menengah','utama') : normal
			// proteksi ('on','off') : off
			
			if(isset($dtmember['token']))
			$data_auth = array_merge($data_auth, array('token'=>$dtmember['token'], 'token_timeout'=>time()+3600));
		
			if($this->CI->db->insert($this->tbl_member, $data_member)) {
				$mem_id =  $this->CI->db->insert_id();
				$data_auth['id_mem'] = $mem_id;
				$this->CI->db->insert($this->tbl_member_auth, $data_auth);
				return $mem_id;
			}
			else
			return false;
		}
		else
			return $cek;
	}
	
	// Form Login
	function cekMemLogin($userid, $kunci, $json_lstakses='') { 
		$cek = '404';
		$now = time();
		$userid = filter_var($userid, FILTER_SANITIZE_STRING);
		$kunci = filter_var($kunci, FILTER_SANITIZE_STRING);
		// Cek db
		$cntlogin = $this->CI->db->count_all($this->tbl_member_auth);
		$login = $this->CI->db->get($this->tbl_member_auth);
		if($cntlogin > 0) {
		foreach($login->result_array() as $row) {
			$match1 = dikode($row['id_member_login'],true);
			$match2 = dikode($row['kunci_pas'],true);
			if($userid === $match1 && $kunci === $match2)
			{
				if($row['status_auth'] == 'on' || $row['status_auth'] == 'fpass') {
					$cek = true;
					$idmem = $row['id_mem'];
					// Get Member Info
					$lsesi_mem = array();
					$mem_info = $this->CI->db->get_where($this->tbl_member, array('id_mem' => $idmem));
					$mem = $mem_info->row_array();
					$lsesi_auth = array('member' => trim($row['id_member_login']), 
								 'mailmem' => trim($row['email_daftar']), 'lvmem' => dikode($row['mem_level']), 
								 'tokenauth' => dikode($row['id_auth']),
								 'tokenidmem' => dikode($idmem));
					if(isset($mem)) {
						$lsesi_mem = array('nmmem' => dikode($mem['nama_panggilan'], true), 'nmfull' => dikode($mem['nama_lengkap'], true), 'tgl_lahir' => strtotime($mem['tgl_lahir']));
					}
					$lsesi = array_merge($lsesi_auth, $lsesi_mem); 
					$this->CI->session->set_userdata($lsesi);
					$data = array('id_member_login' => dikode($userid), 'kunci_pas' => dikode($kunci), 'tslog_akhir' => format_tgl_sql(time()), 'ttl_login'=>$row['ttl_login']+1);
					$data['txt_lstakses'] = $json_lstakses;
					$data['txt_oldakses'] = $row['txt_lstakses'];
					// konfirmasi forget password via email -- DIBATALKAN
					if($row['status_auth'] == 'fpass') { 
						$data['status_auth'] = 'on';
						$data['token'] = '';
						$data['token_timeout'] = 0;
					}
					$this->CI->db->update($this->tbl_member_auth, $data, array('id_auth' => $row['id_auth']));
					//$this->CI->session->set_flashdata('sukses', 'au_sukses');
					break;
				}
				// konfirmasi token via email
				elseif($row['status_auth'] == 'econf') {
					$cek = '403';
					if($row['token_timeout'] >= $now) {
						$idmem = $row['id_mem'];
						$this->CI->db->delete($this->tbl_member_auth, array('id_mem' => $idmem));
						$this->CI->db->delete($this->tbl_member, array('id_mem' => $idmem));
						$cek = '404';
					}					
					break;
				}
				// banned-status:off
				elseif($row['status_auth'] == 'off') {
					$cek = 'banned';			
					break;
				}
				else {
					$cek = false; 
					break;
				}
			}
		}
		}
		return $cek;
	}
	
	// Param $level = sesi('lvmem')
	function cekMemAuth($level, $allow=array('free', 'premium', 'gold')) {
		if($level!='' && $this->CI->session->userdata('member') != '' && $this->CI->session->userdata('tokenauth') != '' && $this->CI->session->userdata('tokenidmem') != '') {
			$tokenid = dikode($this->CI->session->userdata('tokenauth'), true);
			$level = dikode($level, true);
			if(in_array($level, $allow) && is_numeric($tokenid))
				return true;
			else 
				return false;
		}
		else {
			return false;
		}
	}
	
	// Cek sesi akses
	function logMemCheck($url='', $ajax=false, $isadm=false) { 
		if($url=='')
		$url = BASELOGIN;
		// jqXHR.responseText === "logout-ajax";
		$ajax = iniajax();
		
		if(!$isadm && $this->CI->session->userdata('valogmem') != '' && $this->CI->session->userdata('valogmem') === 'ok') {
			if($this->CI->session->userdata('member') != '' && $this->CI->session->userdata('tokenauth') != '' && $this->CI->session->userdata('tokenidmem') != '' && $this->CI->session->userdata('mailmem') != '') {
				$idauth = filter_var(trim($this->CI->session->userdata('tokenauth')), FILTER_SANITIZE_STRING);
				$idmem = filter_var(trim($this->CI->session->userdata('tokenidmem')), FILTER_SANITIZE_STRING);
				$tokena = (int)dikode($idauth, true); 
				$tokenb = (int)dikode($idmem, true);
				$cntlogin = $this->CI->db->count_all($this->tbl_member_auth, " id_auth=$tokena AND id_mem=$tokenb ");
				if($cntlogin === 1)
					return true;
				else {
					if(!$ajax)
					$this->logOutMem('au_gagal1b', $url);
					else
					exit('logout-ajax');
				}
			}
			else {
				if(!$ajax)
				$this->logOutMem('au_gagal1b', $url);
				else
				exit('logout-ajax');
			}
		}
		else {
			if($this->CI->session->userdata('member') != '' && $this->CI->session->userdata('tokenauth') != '' && $this->CI->session->userdata('tokenidmem') != '' && $this->CI->session->userdata('mailmem') != '') {
				$idx = filter_var(trim($this->CI->session->userdata('tokenidmem')), FILTER_SANITIZE_STRING);
				$uid = filter_var(trim($this->CI->session->userdata('member')), FILTER_SANITIZE_STRING);
				$token = (int)dikode($idx, true);
				$userid = (int)dikode($uid, true);
				if(is_numeric($token)) {
					$sql = "SELECT * FROM " . $this->tbl_member_auth . " WHERE id_mem = $token LIMIT 1";
					$cek = $this->CI->db->query($sql);
					$row = $cek->row_array();
					if (isset($row)) {
						//foreach($cek as $row) {
							$id_masuk = (int)dikode($row['id_member_login'], true);
							if($id_masuk == $userid) {
								if($row['status_auth']=='off') {
									if(!$ajax)
									$this->logOutMem('au_gagal1b', $url);
									else
									exit('logout-ajax');
								}
								else {
									$this->CI->session->set_userdata('valog', 'ok');
									return true;
								}
							}
							else {
								if(!$ajax)
								$this->logOutMem('au_gagal1a', $url);
								else
								exit('logout-ajax');
							}
						//}
					}
					else {
						if(!$ajax)
						$this->logOutMem('au_gagal2', $url);
						else
						exit('logout-ajax');
					}
				}
				else {
					if(!$ajax)
					$this->logOutMem('au_gagal3', $url);
					else
					exit('logout-ajax');
				}
			}
			else {
				if(!$ajax)
				$this->logOutMem('au_gagal4', $url);
				else
				exit('logout-ajax');
			}
		}
	}
	
	function logOutMem($msg='', $url='') {
		// off log
		$ladm = array('nmmem','member','mailmem','lvmem','tokenauth','tokenidmem','valogmem');
		$this->CI->session->unset_userdata($ladm);
		if($msg != '')
			$this->CI->session->set_flashdata('warning', $msg);
		if($url=='')
		$url = BASELOGIN;
		if($url != '') {
			redirect($url);
		}
	}
	
	function rmeMemInsert() {
		$token = trim($this->CI->session->userdata('tokenidmem'));
		$token = dikode($token);
		setcookie('nxm1', $token, time() + (86400 * 7), '/');
		setcookie('zxm1', trim($this->CI->session->userdata('member')), time() + (86400 * 7), '/');
	}
	
	function rmeMemDel() {
		setcookie('nxm1', '', time() - 3600, '/');
	}
	
	function rmeMemDelAll() {
		setcookie('nxm1', '', time() - 3600, '/');
		setcookie('zxm1', '', time() - 3600, '/');
	}
	
	function rmeMemCheck() {
		if(isset($_COOKIE['nxm1']) && $_COOKIE['nxm1'] != '' && isset($_COOKIE['zxm1']) && $_COOKIE['zxm1'] != '') {
			$idx = trim($_COOKIE['nxm1']);
			$idx = filter_var($idx, FILTER_SANITIZE_STRING);
			$uid = trim($_COOKIE['zxm1']);
			$uid = filter_var($uid, FILTER_SANITIZE_STRING);
			$token = dikode($idx, true);
			$token = dikode($token, true);
			// 2x enkrip
			$userid = dikode($uid, true); 
			if(is_numeric($token)) {
				$sql = "SELECT * FROM " . $this->tbl_member_auth . " WHERE id_mem = $token LIMIT 1";
				$cek = $this->CI->db->query($sql);
				$row = $cek->row_array();
				if (isset($row)) {
					//foreach($cek as $row) {
						$id_masuk = dikode($row['id_member_login'], true);
						if($id_masuk == $userid)
							return true;
						else {
							$this->rmeMemDel();
							return false;
						}
					//}
				}
				else {
					$this->rmeMemDel();
					return false;
				}
			}
			else {
				$this->rmeMemDel();
				return false;
			}
		}
		else {
			return false;
		}
	}
}