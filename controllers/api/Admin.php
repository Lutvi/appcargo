<?php 

require_once APPPATH . '/libraries/Admin_Controller.php';

class Admin extends Admin_Controller {
	
	function __construct()
  {
		parent::__construct();
		$this->methods['login_post']['limit'] = 100;
		
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('', '<br>');
	}

	public function login_post()
	{
		$this->form_validation->set_rules('username', 'User Name', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		$csrf = array(
					'name' => $this->security->get_csrf_token_name(),
					'hash' => $this->security->get_csrf_hash()
				);
				
		if($this->form_validation->run() == FALSE) {
			$this->set_response([
					'status' => false,
					'code' => REST_Controller::HTTP_BAD_REQUEST,
					'message' => validation_errors(),
					'csrf' => $csrf
			], REST_Controller::HTTP_CREATED);
		}
		else {
			$cek = $this->authAdmin->cekAdmLogin($this->post('username'), $this->post('password'));
			
			if($cek === true) {
				if($this->post('rme')) {
					$this->authAdmin->rmeAdmInsert();
				}
				$this->set_response([
						'status' => true,
						'code' => REST_Controller::HTTP_CREATED,
						'data' => $cek
				], REST_Controller::HTTP_CREATED);
			}
			else {
				if($cek == 'banned') {
					// Remove RME
					$this->authAdmin->rmeMemDelAll();
					$this->set_response([
							'status' => 'banned',
							'code' => REST_Controller::HTTP_UNAUTHORIZED,
							'message' => 'Your account is temporay disabled by webmaster!',
							'csrf' => $csrf
					], REST_Controller::HTTP_CREATED);
				} 
				else {
					$this->set_response([
							'status' => false,
							'code' => REST_Controller::HTTP_UNAUTHORIZED,
							'message' => 'User Name or Password not match!',
							'csrf' => $csrf
					], REST_Controller::HTTP_CREATED);
				}
			}
		}
	}

/*
	public function addadmin_post()
	{
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('fullName', 'Full Name', 'required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]');
		$this->form_validation->set_rules('passconf', 'Repeat Password', 'trim|required|min_length[6]|matches[password]');
		
		$csrf = array(
					'name' => $this->security->get_csrf_token_name(),
					'hash' => $this->security->get_csrf_hash()
				);
				
		if($this->form_validation->run() == FALSE) {
			$this->set_response([
					'status' => false,
					'code' => REST_Controller::HTTP_BAD_REQUEST,
					'message' => validation_errors(),
					'csrf' => $csrf
			], REST_Controller::HTTP_CREATED);
		}
		else {
			$dtadmin = array(
						'email' => $this->post('email'),
						'idmasuk' => $this->post('email'),
						'kunci' => $this->post('password'),
						'level' => 'admin',
						'status' => 'on',
						);
			$idadm = $this->authAdmin->addAdmin($dtadmin);
			
			if(is_numeric($idadm)) {
				$this->set_response([
						'status' => TRUE,
						'code' => REST_Controller::HTTP_CREATED,
						'message' => 'Registration success, please activated your account.'
				], REST_Controller::HTTP_CREATED);
			}
			else {
				$this->set_response([
						'status' => false,
						'code' => REST_Controller::HTTP_NOT_ACCEPTABLE,
						'message' => 'Email already registred!',
						'csrf' => $csrf
				], REST_Controller::HTTP_CREATED);
			}
		}
	}
		
	public function getuser_post()
	{
		$this->form_validation->set_rules('idmem', 'data-idmem', 'trim|required');
		$csrf = array(
					'name' => $this->security->get_csrf_token_name(),
					'hash' => $this->security->get_csrf_hash()
				);
		
		if($this->form_validation->run() == FALSE) {
			$this->set_response([
					'status' => false,
					'code' => REST_Controller::HTTP_BAD_REQUEST,
					'message' => validation_errors(),
					'csrf' => $csrf
			], REST_Controller::HTTP_CREATED);
		}
		else {
			$idmem = bskode($this->post('idmem'), true);
			$cek = $this->db->get_where('tbl_app_member', array('id_mem' => $idmem), 1);
			
			if(isset($cek) && $cek->num_rows() > 0) {
				$row = $cek->row();
				
				$dtmem = array(
						'nick' => dikode($row->nama_panggilan, true),
						'fullName' => dikode($row->nama_lengkap, true),
						'dob' => format_tgl('id', $row->tgl_lahir),
						'gen' => $row->gender,
						'bio' => dikode($row->biodata_profil, true),
						);
						
				$this->set_response([
						'status' => true,
						'code' => REST_Controller::HTTP_CREATED,
						'data' => $dtmem,
						'csrf' => $csrf
				], REST_Controller::HTTP_CREATED);
			}
			else {
				$this->set_response([
						'status' => false,
						'code' => REST_Controller::HTTP_NOT_FOUND,
						'message' => 'Not Found!',
						'csrf' => $csrf
				], REST_Controller::HTTP_CREATED);
			}
		}
	}
	
	// Block
	public function blockuser_post()
	{
		$this->form_validation->set_rules('idauth', 'data-idauth', 'trim|required');
		$this->form_validation->set_rules('sts', 'data-sts', 'trim|required');
		$csrf = array(
					'name' => $this->security->get_csrf_token_name(),
					'hash' => $this->security->get_csrf_hash()
				);
		
		if($this->form_validation->run() == FALSE) {
			$this->set_response([
					'status' => false,
					'code' => REST_Controller::HTTP_BAD_REQUEST,
					'message' => validation_errors(),
					'csrf' => $csrf
			], REST_Controller::HTTP_CREATED);
		}
		else {
			$idauth = bskode($this->post('idauth'), true);
			$status = bskode($this->post('sts'), true);
			$cek = $this->db->get_where('tbl_app_member_auth', array('id_auth' => $idauth), 1);
			if(isset($cek) && $cek->num_rows() > 0) {
				$res = $this->db->update('tbl_app_member_auth', array('status_auth' => $status), array('id_auth' => $idauth));
				if($res) {
					$this->set_response([
							'status' => true,
							'code' => REST_Controller::HTTP_CREATED,
							'data' => $dtmem,
							'csrf' => $csrf
					], REST_Controller::HTTP_CREATED);
				}
				else {
					$this->set_response([
							'status' => false,
							'code' => REST_Controller::HTTP_REQUEST_TIMEOUT,
							'message' => 'Error while updating data.',
							'csrf' => $csrf
					], REST_Controller::HTTP_CREATED);
				}
			}
			else {
				$this->set_response([
						'status' => false,
						'code' => REST_Controller::HTTP_NOT_FOUND,
						'message' => 'Not Found!',
						'csrf' => $csrf
				], REST_Controller::HTTP_CREATED);
			}
		}
	}
	
	// Email
	public function emailuser_post()
	{
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('subject', 'Subject', 'required');
		$this->form_validation->set_rules('message', 'Message', 'required');
		$csrf = array(
					'name' => $this->security->get_csrf_token_name(),
					'hash' => $this->security->get_csrf_hash()
				);
		
		if($this->form_validation->run() == FALSE) {
			$this->set_response([
					'status' => false,
					'code' => REST_Controller::HTTP_BAD_REQUEST,
					'message' => validation_errors(),
					'csrf' => $csrf
			], REST_Controller::HTTP_CREATED);
		}
		else {
			$this->load->library('email');
			$email = $this->post('email');
			$subject = $this->post('subject');
			$message = $this->post('message');
			
			$this->email->from('webmaster@example.com', 'Admin');
			$this->email->to($email);
			$this->email->subject($subject);
			$this->email->message($message);
			//$tes = $this->email->send();
			$tes = true;
			if($tes) {
				$this->set_response([
							'status' => true,
							'code' => REST_Controller::HTTP_CREATED,
							'data' => 'success',
							'csrf' => $csrf
					], REST_Controller::HTTP_CREATED);
			}
			else {
				$this->set_response([
							'status' => false,
							'code' => REST_Controller::HTTP_REQUEST_TIMEOUT,
							'message' => 'Error while send email!',
							'csrf' => $csrf
					], REST_Controller::HTTP_CREATED);
			}
		}
	}
*/

}
