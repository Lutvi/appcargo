<?php 

require APPPATH . '/libraries/User_Controller.php';

class User extends User_Controller {
	
	function __construct()
  {
		parent::__construct();
		$this->methods['login_post']['limit'] = 100;
		
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('', '<br>');
	}
	
	public function registration_post()
	{
		//$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('nama', 'Full Name', 'required');
		$this->form_validation->set_rules('username', 'User Name', 'required');
		$this->form_validation->set_rules('wilayah', 'Wilayah', 'required');
		$this->form_validation->set_rules('level', 'Level', 'required');
		$this->form_validation->set_rules('kode_pos', 'Kodepos', 'required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]');
		$this->form_validation->set_rules('passconf', 'Repeat Password', 'trim|required|min_length[6]|matches[password]');
		
		$csrf = array(
					'name' => $this->security->get_csrf_token_name(),
					'hash' => $this->security->get_csrf_hash()
				);
				
		if($this->form_validation->run() == FALSE) {
			$msg = explode("<br>\n", validation_errors());
			$this->set_response([
					'status' => false,
					'code' => REST_Controller::HTTP_BAD_REQUEST,
					'message' => remove_empty($msg),
					'csrf' => $csrf
			], REST_Controller::HTTP_CREATED);
		}
		else {
			$dtmember = array(
						'nama' => $this->post('nama'),
						'username' => $this->post('username'),
						'password' => $this->post('password'),
						'wilayah' => $this->post('wilayah'),
						'level' => $this->post('level'),
						'kode_pos' => $this->post('kode_pos'),
						);
			$idmem = $this->authUser->addMember($dtmember);
			
			if(is_numeric($idmem)) {
				$this->set_response([
						'status' => TRUE,
						'code' => REST_Controller::HTTP_CREATED,
						'message' => 'Registration success, please login with your account.'
				], REST_Controller::HTTP_CREATED);
			}
			else {
				$this->set_response([
						'status' => false,
						'code' => REST_Controller::HTTP_NOT_ACCEPTABLE,
						'message' => 'Username or Name already registred!',
						'csrf' => $csrf
				], REST_Controller::HTTP_CREATED);
			}
		}
	}
	
	public function login_post()
	{
		$this->form_validation->set_rules('username', 'User Name', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]');
		$csrf = array(
					'name' => $this->security->get_csrf_token_name(),
					'hash' => $this->security->get_csrf_hash()
				);
				
		if($this->form_validation->run() == FALSE) {
			$msg = explode("<br>\n", validation_errors());
			$this->set_response([
					'status' => false,
					'code' => REST_Controller::HTTP_BAD_REQUEST,
					'message' => remove_empty($msg),
					'csrf' => $csrf
			], REST_Controller::HTTP_CREATED);
		}
		else {
			$cek = $this->authUser->cekMemLogin($this->post('username'), $this->post('password'));
			
			if($cek === true) {
				if($this->post('rme')) {
					$this->authUser->rmeMemInsert();
				}
				$this->set_response([
						'status' => true,
						'code' => 'ok-login',
						'data' => $cek
				], REST_Controller::HTTP_CREATED);
			}
			else {
				if($cek == 'banned') {
					// Remove RME
					$this->authUser->rmeMemDelAll();
					$this->set_response([
							'status' => 'banned',
							'code' => REST_Controller::HTTP_UNAUTHORIZED,
							'message' => 'Your account is temporay disabled by admin!',
							'csrf' => $csrf
					], REST_Controller::HTTP_CREATED);
				} 
				else {
					$this->set_response([
							'status' => false,
							'code' => REST_Controller::HTTP_NOT_FOUND,
							'message' => 'Email or Password not match!',
							'csrf' => $csrf
					], REST_Controller::HTTP_CREATED);
				}
			}
		}
	}
	
	public function fpass_post()
	{
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$csrf = array(
					'name' => $this->security->get_csrf_token_name(),
					'hash' => $this->security->get_csrf_hash()
				);
		
		if($this->form_validation->run() == FALSE) {
			$msg = explode("<br>\n", validation_errors());
			$this->set_response([
					'status' => false,
					'code' => REST_Controller::HTTP_BAD_REQUEST,
					'message' => remove_empty($msg),
					'csrf' => $csrf
			], REST_Controller::HTTP_CREATED);
		}
		else {
			$found = false;
			$idmem = 0;
			$email = $this->post('email');
			$this->db->select('id_mem, id_member_login, email_daftar');
			$cek = $this->db->get('tbl_app_member_auth');
			if($cek->num_rows() > 0) {
				foreach ($cek->result() as $row) {
					$cek_email = dikode($row->email_daftar, true);
					if($cek_email == $email){
						$idmem = $row->id_mem;
						$found = true;
						break;
					}
				}
			}
			
			if($found) { 
				$url = BASE.'app/login/confirm/';
				$token = randomPassword(8);
				$link = $this->authUser->buildForgetUrlMem($url, $idmem, $token);
				
				$this->set_response([
						'status' => true,
						'code' => REST_Controller::HTTP_CREATED,
						'message' => $link
				], REST_Controller::HTTP_CREATED);
			}
			else {
				$this->set_response([
						'status' => false,
						'code' => REST_Controller::HTTP_UNAUTHORIZED,
						'message' => 'Email not registred!',
						'csrf' => $csrf
				], REST_Controller::HTTP_CREATED);
			}
		}
	}
	
	public function upprofile_post()
	{
		$this->form_validation->set_rules('fullName', 'Full Name', 'required');
		$this->form_validation->set_rules('dob', 'Date of Birth', 'required');
		$this->form_validation->set_rules('gender', 'Gender', 'required');
		$csrf = array(
					'name' => $this->security->get_csrf_token_name(),
					'hash' => $this->security->get_csrf_hash()
				);
				
		if($this->form_validation->run() == FALSE) {
			$msg = explode("<br>\n", validation_errors());
			$this->set_response([
					'status' => false,
					'code' => REST_Controller::HTTP_BAD_REQUEST,
					'message' => remove_empty($msg),
					'csrf' => $csrf
			], REST_Controller::HTTP_CREATED);
		}
		else {
			$idmem = dikode($this->session->userdata('tokenidmem'), true);
			$cek = $this->db->get_where('tbl_app_member', array('id_mem' => $idmem));
			
			if($cek->num_rows() > 0) {
				$dtmember = array(
						'nama_panggilan' => dikode($this->post('fullName')),
						'nama_lengkap' => dikode($this->post('fullName')),
						'tgl_lahir' => $this->post('dob'),
						'gender' => $this->post('gender'),
						'biodata_profil' => dikode($this->post('bio')),
						);
				$this->db->update('tbl_app_member', $dtmember, array('id_mem' => $idmem));
				$this->db->update('tbl_app_member_auth', array('tgl_update_data' => date('Y-m-d H:i:s')), array('id_mem' => $idmem));
				
				$this->set_response([
						'status' => true,
						'code' => REST_Controller::HTTP_CREATED,
						'message' => $cek
				], REST_Controller::HTTP_CREATED);
			}
			else {
				$this->set_response([
						'status' => false,
						'code' => REST_Controller::HTTP_UNAUTHORIZED,
						'message' => 'Unknown Error!',
						'csrf' => $csrf
				], REST_Controller::HTTP_CREATED);
			}
		}
	}
	
	public function chpass_post()
	{
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]');
		$this->form_validation->set_rules('npassword', 'New Password', 'trim|required|min_length[6]');
		$this->form_validation->set_rules('npassconf', 'Repeat New Password', 'trim|required|min_length[6]|matches[npassword]');
		$csrf = array(
					'name' => $this->security->get_csrf_token_name(),
					'hash' => $this->security->get_csrf_hash()
				);
				
		if($this->form_validation->run() == FALSE) {
			$msg = explode("<br>\n", validation_errors());
			$this->set_response([
					'status' => false,
					'code' => REST_Controller::HTTP_BAD_REQUEST,
					'message' => remove_empty($msg),
					'csrf' => $csrf
			], REST_Controller::HTTP_CREATED);
		}
		else {
			// Var-Id
			$idauth = dikode($this->session->userdata('tokenauth'), true);
			$userid = dikode($this->session->userdata('member'), true);
			// Cek
			$cek = $this->authUser->cekMemLogin($userid, $this->post('password'));
			$cekdt = $this->db->get_where('tbl_user_mobile', array('id_user_mobile' => $idauth));
			
			if($cek === true && $cekdt->num_rows() > 0) {
				// Do Update pass
				$dtauth = array(
						'password' => dikode($this->post('npassword'))
						);
				$this->db->update('tbl_user_mobile', $dtauth, array('id_user_mobile' => $idauth));
				
				$this->set_response([
						'status' => true,
						'code' => REST_Controller::HTTP_CREATED,
						'message' => 'Your password, successfully changed.'
				], REST_Controller::HTTP_CREATED);
			}
			else {
				$this->set_response([
						'status' => false,
						'code' => REST_Controller::HTTP_UNAUTHORIZED,
						'message' => 'Password not match!',
						'csrf' => $csrf
				], REST_Controller::HTTP_CREATED);
			}
		}
	}
	
}
