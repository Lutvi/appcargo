<?php 

class Tabel extends MY_Controller {

	public $perpage = 20;
	
	public function __construct()
	{
		parent::__construct();

		// Check Auth
		$this->authAdmin->logAdmCheck(BASELOGIN);

		// Lib
		$this->load->library('pagination');

		// Models
		$this->load->model('admin/Customer_model', 'mcustomer');
		$this->load->model('admin/Penerima_model', 'mpenerima');
		$this->load->model('admin/Tarif_model', 'mtarif');
		$this->load->model('admin/Transaksi_model', 'mtransaksi');
	}
	
	public function index() 
	{
		$data['title'] = 'Contoh Tabel';
		$data['content'] = 'contoh_tabel';
		$this->load->view('admin/main_tabel', $data);
	}

	public function customer() 
	{
		$data['title'] = 'Daftar Customer';
		$data['content'] = 'customer_tabel';

		// Pagination
		$config['base_url'] = base_url('admin/tabel/customer');
		$config['total_rows'] = $this->mcustomer->countAllPelanggan();
		$config['per_page'] = $this->perpage;
		$config['uri_segment'] = 4;
		$this->pagination->initialize($config);
		$this->pagination->create_links();

		// Attr Daftar
		$filter = array();
		$orderby = 'nama_pelanggan';
		$dirorder = 'ASC';
		$limit = $this->perpage;
		$offset = $this->getOffsetPage($this->uri->segment($config['uri_segment']));
		$data['daftar'] = $this->mcustomer->getAllPelanggan($filter, $orderby, $dirorder, $limit, $offset);


		//tsout($data);
		$this->load->view('admin/main_tabel', $data);
	}

	public function penerima() 
	{
		$data['title'] = 'Daftar Penerima';
		$data['content'] = 'penerima_tabel';

		// Pagination
		$config['base_url'] = base_url('admin/tabel/penerima');
		$config['total_rows'] = $this->mpenerima->countAllPenerima();
		$config['per_page'] = $this->perpage;
		$config['uri_segment'] = 4;
		$this->pagination->initialize($config);
		$this->pagination->create_links();

		// Attr Daftar
		$filter = array();
		$orderby = 'nama_penerima';
		$dirorder = 'ASC';
		$limit = $this->perpage;
		$offset = $this->getOffsetPage($this->uri->segment($config['uri_segment']));
		$data['daftar'] = $this->mpenerima->getAllPenerima($filter, $orderby, $dirorder, $limit, $offset);

		$this->load->view('admin/main_tabel', $data);
	}

	public function tarif() 
	{
		$data['title'] = 'Daftar Tarif';
		$data['content'] = 'tarif_tabel';
		
		// Pagination
		$config['base_url'] = base_url('admin/tabel/tarif');
		$config['total_rows'] = $this->mtarif->countAllTarif();
		$config['per_page'] = $this->perpage;
		$config['uri_segment'] = 4;
		$this->pagination->initialize($config);
		$this->pagination->create_links();

		// Attr Daftar
		$filter = array();
		$orderby = 'kode_pos';
		$dirorder = 'ASC';
		$limit = $this->perpage;
		$offset = $this->getOffsetPage($this->uri->segment($config['uri_segment']));
		$data['daftar'] = $this->mtarif->getAllTarif($filter, $orderby, $dirorder, $limit, $offset);
		
		$this->load->view('admin/main_tabel', $data);
	}

	public function transaksi() 
	{
		$data['title'] = 'Daftar Transaksi';
		$data['content'] = 'transaksi_tabel';
		$this->load->view('admin/main_tabel', $data);
	}
	
}