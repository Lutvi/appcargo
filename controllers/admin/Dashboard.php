<?php 

class Dashboard extends MY_Controller {

	public function __construct() 
	{
		parent::__construct();
		
		// Check Auth
		$this->authAdmin->logAdmCheck(BASELOGIN);
		
		// Load Model
		//$this->load->model('admin/CmsMember_model', 'adminModel');
		#tsout($this->session->all_userdata());
	}
	
	public function index() 
	{
		$data['title'] = 'Dashboard';
		$data['content'] = 'dashboard';
		$this->load->view('admin/main_page1', $data);
	}
	
}