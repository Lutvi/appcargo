<?php 

class Logout extends MY_Controller {

	public $authMember;
	
	function __construct()
	{
		parent::__construct();
	}
	
	public function index() 
	{
		$url = base_url('admin/login');
		$this->authAdmin->logOutAdm('', $url);
	}

}

