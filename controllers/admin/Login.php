<?php 

class Login extends MY_Controller {

	public $authAdmin;
	
	public function __construct()
	{
		parent::__construct();
		
		// Load Lib authMember
		$this->load->library('authAdmin');
		$this->authAdmin = new authAdmin();

		// user: admin@admin.com
		// pass: 123456
		/* New
		$dtadmin = array(
						'nama' => 'Admin',
						'email' => 'admin@admin.com',
						'idmasuk' => 'admin',
						'kunci' => '123456',
						'level' => 'super',
						'status' => 'on',
						);
		$idadm = $this->authAdmin->addAdmin($dtadmin);
		tsout($idadm);
		$this->authAdmin->cekAdmLogin('admin', '123456');*/
	}
	
	public function index() {
		$data['unamex'] = '';
		// RME
		if(isset($_COOKIE['zx']) && $_COOKIE['zx'] != '')
		$data['unamex'] = dikode(trim($_COOKIE['zx']),true);
		
		#tsout($this->session->all_userdata());

		// Check Auth
		if($this->session->userdata('lvadm'))
		redirect('/admin/dashboard');
		else
		$this->load->view('admin/login', $data);
	}

}