<?php 

class Form extends MY_Controller {
	
	public function __construct()
	{
		parent::__construct();
		
		// Check Auth
		$this->authAdmin->logAdmCheck(BASELOGIN);

		// Models
		$this->load->model('admin/Customer_model', 'mcustomer');
		$this->load->model('admin/Penerima_model', 'mpenerima');
		$this->load->model('admin/Tarif_model', 'mtarif');
		$this->load->model('admin/Transaksi_model', 'mtransaksi');

		// Form
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
	}
	
	public function index() 
	{
		$data['title'] = 'Contoh Form';
		$data['content'] = 'contoh_form';
		$this->load->view('admin/main_form', $data);
	}

// CUSTOMER
	public function customer() 
	{
		$data['title'] = 'Customer';
		$data['content'] = 'customer_form';
		$data['aksi'] = 'add';
		$this->load->view('admin/main_form', $data);
	}
	
	public function add_customer() 
	{
		$data['title'] = 'Customer';
		$data['content'] = 'customer_form';
		$data['aksi'] = 'add';

		// Validation
		$this->form_validation->set_rules(
						'kode_pelanggan', 'Kode Pelanggan',
						array(
								'required',
								array('kode_pelanggan_callable', array($this->mvalidasi, 'cekKodePelanggan'))
						)
		);
		$this->form_validation->set_message('kode_pelanggan_callable', 'Kode Pelanggan already exists');
		$this->form_validation->set_rules(
						'nama_pelanggan', 'Nama Pelanggan',
						array(
								'required',
								array('nama_pelanggan_callable', array($this->mvalidasi, 'cekNamaPelanggan'))
						)
		);
		$this->form_validation->set_message('nama_pelanggan_callable', 'Nama Pelanggan already exists');
		// -----------
		$this->form_validation->set_rules('status_langganan', 'Status Langganan', 'required');
		$this->form_validation->set_rules('alamat[]', 'Alamat', 'trim');
		// End-Validation

		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('admin/main_form', $data);
		}
		else {
			$data = array(
        'kode_pelanggan' => $this->input->post('kode_pelanggan'),
        'nama_pelanggan' => $this->input->post('nama_pelanggan'),
        'no_telepon' => $this->input->post('telepon'),
        'fax' => $this->input->post('fax'),
        'email' => $this->input->post('email'),
        'status_langganan' => $this->input->post('status_langganan'),
        'alamat' => format_alamat($this->input->post('alamat[]')),
			);

			if($this->mcustomer->simpanPelanggan($data))
				redirect('admin/tabel/customer');
			else {
				$this->form_validation->set_message('rule', 'Gagal menyimpan ke database');
				$this->load->view('admin/main_form', $data);
			}
		}
	}

	public function up_customer() 
	{
		$data['title'] = 'Customer';
		$data['content'] = 'customer_upform';
		$data['aksi'] = 'up';
		
		$id = $this->uri->segment(4);
		$data['id'] = $id;
		$getdata = $this->mcustomer->getPelangganById($id);
		$data['isidata'] = $getdata;

		// Handling empty ROW
		if($getdata === null)
			redirect('admin/tabel/customer');

		// Hidden field
		$this->form_validation->set_rules('id_customer', 'ID', 'required');
		$this->form_validation->set_rules('ori_nama_pelanggan', 'Orig Nama', 'required');
		$this->form_validation->set_rules('ori_kode_pelanggan', 'Orig Kode', 'required');

		// Validation
		$this->form_validation->set_rules(
						'kode_pelanggan', 'Kode Pelanggan',
						array(
								'required',
								array('kode_pelanggan_callable', array($this->mvalidasi, 'cekUpKodePelanggan'))
						)
		);
		$this->form_validation->set_message('kode_pelanggan_callable', 'Kode Pelanggan already exists');
		$this->form_validation->set_rules(
						'nama_pelanggan', 'Nama Pelanggan',
						array(
								'required',
								array('nama_pelanggan_callable', array($this->mvalidasi, 'cekUpNamaPelanggan'))
						)
		);
		$this->form_validation->set_message('nama_pelanggan_callable', 'Nama Pelanggan already exists');
		// -----------
		$this->form_validation->set_rules('status_langganan', 'Status Langganan', 'required');
		$this->form_validation->set_rules('alamat[]', 'Alamat', 'trim');
		// End-Validation

		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('admin/main_form', $data);
		}
		else
		{
			$data = array(
        'kode_pelanggan' => $this->input->post('kode_pelanggan'),
        'nama_pelanggan' => $this->input->post('nama_pelanggan'),
        'no_telepon' => $this->input->post('telepon'),
        'fax' => $this->input->post('fax'),
        'email' => $this->input->post('email'),
        'status_langganan' => $this->input->post('status_langganan'),
        'alamat' => format_alamat($this->input->post('alamat[]')),
			);

			if($this->mcustomer->updatePelanggan($data, $id))
				redirect('admin/tabel/customer');
			else {
				$this->form_validation->set_message('rule', 'Gagal menyimpan ke database');
				$this->load->view('admin/main_form', $data);
			}
		}
	}
// END-CUSTOMER

// PENERIMA
	public function penerima() 
	{
		$data['title'] = 'Penerima';
		$data['content'] = 'penerima_form';
		$data['aksi'] = 'add';
		$data['list_customer'] = $this->mcustomer->getListPelanggan();

		$this->load->view('admin/main_form', $data);
	}
	
	public function add_penerima() 
	{
		$data['title'] = 'Penerima';
		$data['content'] = 'penerima_form';
		$data['aksi'] = 'add';
		$data['list_customer'] = $this->mcustomer->getListPelanggan();

		// Validation
		$this->form_validation->set_rules('customer', 'Customer', 'integer|required');
		$this->form_validation->set_rules(
						'nama_penerima', 'Nama Penerima',
						array(
							'required',
							array('nama_penerima_callable', array($this->mvalidasi, 'cekNamaPenerima'))
						)
		);
		$this->form_validation->set_message('nama_penerima_callable', 'Nama Penerima already exists');
		// -----------
		$this->form_validation->set_rules('kodepos', 'Kodepos', 'required');
		$this->form_validation->set_rules('kota', 'Kota', 'required');
		$this->form_validation->set_rules('alamat[]', 'Alamat', 'trim');
		// End-Validation

		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('admin/main_form', $data);
		}
		else {
			$data = array(
        'id_customer' => $this->input->post('customer'),
        'nama_penerima' => $this->input->post('nama_penerima'),
        'kode_pos' => $this->input->post('kodepos'),
        'telepon' => $this->input->post('telepon'),
        'fax' => $this->input->post('fax'),
        'email' => $this->input->post('email'),
        'kota' => $this->input->post('kota'),
        'alamat' => format_alamat($this->input->post('alamat[]')),
			);

			if($this->mpenerima->simpanPenerima($data))
				redirect('admin/tabel/penerima');
			else {
				$this->form_validation->set_message('rule', 'Gagal menyimpan ke database');
				$this->load->view('admin/main_form', $data);
			}
		}
	}

	public function up_penerima() 
	{
		$data['title'] = 'Penerima';
		$data['content'] = 'penerima_upform';
		$data['aksi'] = 'up';
		$data['list_customer'] = $this->mcustomer->getListPelanggan();

		$id = $this->uri->segment(4);
		$data['id'] = $id;
		$getdata = $this->mpenerima->getPenerimaById($id);
		$data['isidata'] = $getdata;

		// Handling empty ROW
		if($getdata === null)
			redirect('admin/tabel/penerima');

		// Hidden field
		$this->form_validation->set_rules('id_penerima', 'ID', 'required');
		$this->form_validation->set_rules('ori_nama_penerima', 'Orig Nama', 'required');

		// Validation
		$this->form_validation->set_rules('customer', 'Customer', 'integer|required');
		$this->form_validation->set_rules(
						'nama_penerima', 'Nama Penerima',
						array(
							'required',
							array('nama_penerima_callable', array($this->mvalidasi, 'cekUpNamaPenerima'))
						)
		);
		$this->form_validation->set_message('nama_penerima_callable', 'Nama Penerima already exists');
		// -----------
		$this->form_validation->set_rules('kodepos', 'Kodepos', 'required');
		$this->form_validation->set_rules('kota', 'Kota', 'required');
		$this->form_validation->set_rules('alamat[]', 'Alamat', 'trim');
		// End-Validation

		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('admin/main_form', $data);
		}
		else {
			$data = array(
        'id_customer' => $this->input->post('customer'),
        'nama_penerima' => $this->input->post('nama_penerima'),
        'kode_pos' => $this->input->post('kodepos'),
        'telepon' => $this->input->post('telepon'),
        'fax' => $this->input->post('fax'),
        'email' => $this->input->post('email'),
        'kota' => $this->input->post('kota'),
        'alamat' => format_alamat($this->input->post('alamat[]')),
			);

			if($this->mpenerima->updatePenerima($data, $id))
				redirect('admin/tabel/penerima');
			else {
				$this->form_validation->set_message('rule', 'Gagal menyimpan ke database');
				$this->load->view('admin/main_form', $data);
			}
		}
	}
// END-PENERIMA

// TARIF
	public function tarif() 
	{
		$data['title'] = 'Tarif';
		$data['content'] = 'tarif_form';
		$data['aksi'] = 'add';
		$this->load->view('admin/main_form', $data);
	}
	
	public function add_tarif() 
	{
		$data['title'] = 'Tarif';
		$data['content'] = 'tarif_form';
		$data['aksi'] = 'add';

		// Validation
		$this->form_validation->set_rules(
						'kode_tarif', 'Kode Tarif',
						array(
							'required',
							array('kode_tarif_callable', array($this->mvalidasi, 'cekKodeTarif'))
						)
		);
		$this->form_validation->set_message('kode_tarif_callable', 'Kode Tarif already exists');
		// -----------
		$this->form_validation->set_rules('kodepos', 'Kodepos', 'required');
		$this->form_validation->set_rules('service', 'Service', 'required');
		// End-Validation

		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('admin/main_form', $data);
		}
		else {
			$data = array(
        'kode_tarif' => $this->input->post('kode_tarif'),
        'kode_pos' => $this->input->post('kodepos'),
        'harga_layanan_1' => $this->input->post('harga1'),
        'harga_layanan_2' => $this->input->post('harga2'),
        'harga_layanan_3' => $this->input->post('harga3'),
        'diskon_1' => $this->input->post('diskon1'),
        'diskon_2' => $this->input->post('diskon2'),
        'diskon_3' => $this->input->post('diskon3'),
        'service' => $this->input->post('service')
			);

			if($this->mtarif->simpanTarif($data))
				redirect('admin/tabel/tarif');
			else {
				$this->form_validation->set_message('rule', 'Gagal menyimpan ke database');
				$this->load->view('admin/main_form', $data);
			}
		}
	}

	public function up_tarif() 
	{
		$data['title'] = 'Tarif';
		$data['content'] = 'tarif_upform';
		$data['aksi'] = 'up';

		$id = $this->uri->segment(4);
		$data['id'] = $id;
		$getdata = $this->mtarif->getTarifById($id);
		$data['isidata'] = $getdata;

		// Handling empty ROW
		if($getdata === null)
			redirect('admin/tabel/tarif');

		// Hidden field
		$this->form_validation->set_rules('id_tarif', 'ID', 'required');
		$this->form_validation->set_rules('ori_kode_tarif', 'Orig Kode', 'required');

		// Validation
		$this->form_validation->set_rules(
						'kode_tarif', 'Kode Tarif',
						array(
							'required',
							array('kode_tarif_callable', array($this->mvalidasi, 'cekUpKodeTarif'))
						)
		);
		$this->form_validation->set_message('kode_tarif_callable', 'Kode Tarif already exists');
		// -----------
		$this->form_validation->set_rules('kodepos', 'Kodepos', 'required');
		$this->form_validation->set_rules('service', 'Service', 'required');
		// End-Validation

		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('admin/main_form', $data);
		}
		else {
			$data = array(
        'kode_tarif' => $this->input->post('kode_tarif'),
        'kode_pos' => $this->input->post('kodepos'),
        'harga_layanan_1' => $this->input->post('harga1'),
        'harga_layanan_2' => $this->input->post('harga2'),
        'harga_layanan_3' => $this->input->post('harga3'),
        'diskon_1' => $this->input->post('diskon1'),
        'diskon_2' => $this->input->post('diskon2'),
        'diskon_3' => $this->input->post('diskon3'),
        'service' => $this->input->post('service')
			);

			if($this->mtarif->updateTarif($data, $id))
				redirect('admin/tabel/tarif');
			else {
				$this->form_validation->set_message('rule', 'Gagal menyimpan ke database');
				$this->load->view('admin/main_form', $data);
			}
		}
	}
// END-TARIF

// TRANSAKSI
	public function transaksi() 
	{
		$data['title'] = 'Transaksi';
		$data['content'] = 'transaksi_form';
		$data['content_js'] = 'transaksi_form_js';
		$data['aksi'] = 'add';
		$data['list_customer'] = $this->mcustomer->getListPelanggan();
		$data['list_penerima'] = $this->mpenerima->getListPenerima();
		$data['list_tarif'] = $this->mtarif->getListTarif();

		
		$this->load->view('admin/main_form', $data);
	}
	
	public function add_transaksi() 
	{
		$data['title'] = 'Transaksi';
		$data['content'] = 'transaksi_form';
		$data['content_js'] = 'transaksi_form_js';
		$data['aksi'] = 'add';
		$data['list_customer'] = $this->mcustomer->getListPelanggan();
		$data['list_penerima'] = $this->mpenerima->getListPenerima();
		$data['list_tarif'] = $this->mtarif->getListTarif();

		// Validation
		$this->form_validation->set_rules(
						'no_resi', 'No Resi',
						array(
							'required',
							array('no_resi_callable', array($this->mvalidasi, 'cekNoResi'))
						)
		);
		$this->form_validation->set_message('no_resi_callable', 'Nomor Resi already exists');
		// -----------
		$this->form_validation->set_rules('kodepos', 'Kodepos', 'required');
		$this->form_validation->set_rules('service', 'Service', 'required');
		// End-Validation

		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('admin/main_form', $data);
		}
		else {
			$data = array(
        'id_penerima' => $this->input->post('kode_tarif'),
        'id_pengirim' => $this->input->post('kode_tarif'),
        'id_tarif' => $this->input->post('kode_tarif'),
        'no_resi' => $this->input->post('kode_tarif'),
        'tarif' => $this->input->post('kodepos'),
        'diskon' => $this->input->post('harga1'),
        'berat' => $this->input->post('harga2'),
        'tgl_transaksi' => $this->input->post('harga3'),
        'tgl_update' => $this->input->post('diskon1'),
        'modify_by' => $this->input->post('diskon2'),
        'status_update' => $this->input->post('diskon3'),
        'note' => $this->input->post('service'),
        'koli' => $this->input->post('service'),
        'isi_barang_titipan' => $this->input->post('service'),
        'cara_bayar' => $this->input->post('service'),
        'status_bayar' => $this->input->post('service'),
        'total_bayar' => $this->input->post('service'),
        'sisa_bayar' => $this->input->post('service'),
			);

			if($this->mtransaksi->simpanTransaksi($data))
				redirect('admin/tabel/transaksi');
			else {
				$this->form_validation->set_message('rule', 'Gagal menyimpan ke database');
				$this->load->view('admin/main_form', $data);
			}
		}
	}

	public function up_transaksi() 
	{
		$data['title'] = 'Transaksi';
		$data['content'] = 'transaksi_upform';
		$data['content_js'] = 'transaksi_upform_js';
		$data['aksi'] = 'up';
		$data['list_customer'] = $this->mcustomer->getListPelanggan();
		$data['list_penerima'] = $this->mpenerima->getListPenerima();
		$data['list_tarif'] = $this->mtarif->getListTarif();

		
		$this->load->view('admin/main_upform', $data);
	}
// END-TRANSAKSI


}

