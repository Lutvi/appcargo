<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
	
	public $authMember;
	public $authAdmin;

	public function __construct()
	{
		// Construct the parent class
		parent::__construct();
		
		// Load Lib authMember
		/*$this->load->library('authMember');
		$this->authMember = new authMember();*/
		
		// Load Lib authAdmin
		$this->load->library('authAdmin');
		$this->authAdmin = new authAdmin();
	}

	// mencari nilai offset untuk pagination dgn set "use_page_numbers" => TRUE
	public function getOffsetPage($segment, $per='')
	{
		if ( $segment === FALSE || $segment == 1 || $segment == 0 )
		{
			$offset = 0;
		}
		else {
			$per_page = ($per==''?$this->perpage:$per);
			$offset = ($segment-1)*$per_page;
		}

		return $offset;
	}

}